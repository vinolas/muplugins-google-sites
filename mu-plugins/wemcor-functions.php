<?php
/*
Plugin Name: Wemcor Functions
Plugin URI:
Description: Funciones adicionales para los plugins de wemcor
Author: Wemcor
Author URI: https://wemcor.com
Version: 2.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*
 * Load Text Domain
 * Load the plugin textdomain.
 */
add_action( 'muplugins_loaded', 'wemcor_load_text_domain' );
function wemcor_load_text_domain() {
	$plugin_url = plugin_dir_url( __FILE__ );
    load_muplugin_textdomain( 'wemcor-multisite', 'languages' );
}

/*
 * Enqueues
 */
add_action( 'admin_print_styles', 'wemcor_enqueue_styles' );
function wemcor_enqueue_styles() {
	$plugin_url = plugin_dir_url( __FILE__ );
	wp_enqueue_style( 'wemcor-styles', $plugin_url.'assets/style.css' );
}

add_action( 'admin_enqueue_scripts', 'wemcor_enqueue_scripts' );
function wemcor_enqueue_scripts() {
	$plugin_url = plugin_dir_url( __FILE__ );
    wp_register_script('custom-scripts', $plugin_url.'assets/scripts.js', array('jquery'), '2.0', true );
    wp_enqueue_script('custom-scripts');
}

add_action( 'wp_enqueue_scripts', 'wemcor_enqueue_frontend', 999 );
function wemcor_enqueue_frontend() {
	$blog_id = get_current_blog_id();
	$activate_theme = get_blog_option( $blog_id, 'stylesheet');
    wp_enqueue_style('frontend-styles', network_site_url('/wp-content/mu-plugins/assets/'.$activate_theme.'.css'));
}

// cargar título predeterminado cuando se hace click en nueva página
add_filter( 'default_title', 'wemcor_default_title_on_page', 10, 2 );
function wemcor_default_title_on_page( $title, $post ) {
	if( 'page' === $post->post_type ) $title = __('New page', 'wemcor-multisite');
	return $title;
}

// cargar contenido predeterminado cuando se hace click en añadir nueva página
add_filter( 'default_content', 'wemcor_default_content_on_page', 10, 2 );
function wemcor_default_content_on_page( $content, $post ) {
    if( 'page' === $post->post_type ) {
    	$content = '<!-- wp:generateblocks/container {"uniqueId":"a4c6887b","containerWidth":960,"paddingTop":"180","paddingRight":"100","paddingBottom":"180","paddingLeft":"100","paddingTopTablet":"120","paddingBottomTablet":"120","paddingTopMobile":"60","paddingRightMobile":"20","paddingBottomMobile":"60","paddingLeftMobile":"20","backgroundColor":"#226e93","gradient":true,"gradientDirection":0,"gradientColorOne":"#242424","gradientColorOneOpacity":0.4,"gradientColorStopOne":0,"gradientColorTwo":"#242424","gradientColorTwoOpacity":0.4,"gradientColorStopTwo":100,"bgImage":{"id":143,"image":{"url":"%%DOMAIN%%/encabezamiento-1.png","height":544,"width":725,"orientation":"landscape"}},"bgOptions":{"selector":"pseudo-element","opacity":1,"overlay":false,"position":"center center","size":"cover","repeat":"no-repeat","attachment":""},"showAdvancedTypography":true,"align":"full","isDynamic":true} -->
<!-- wp:heading {"textAlign":"center","level":1,"style":{"typography":{"fontSize":80}},"textColor":"white"} -->
<h1 class="has-text-align-center has-white-color has-text-color" style="font-size:80px">Title page</h1>
<!-- /wp:heading -->
<!-- /wp:generateblocks/container -->

<!-- wp:paragraph -->
<p>Start writing the content of the page here. You can include images and links</p>
<!-- /wp:paragraph -->';

	$content = str_replace('%%DOMAIN%%', 'https://'.$_ENV["WORDPRESS_DOMAIN_CURRENT_SITE"].'/wp-content/mu-plugins/images', $content);

	}


    return $content;
}

//prevenir Cannot modify header information - headers already
add_action( 'wp_loaded', 'wemcor_prebvent_headers_custom_redirect' );
function wemcor_prebvent_headers_custom_redirect() {
	if ( is_admin() && isset( $_REQUEST['action'] ) && 'add-site' === $_REQUEST['action'] ) ob_start();
}

//remove notices in admin panel
add_action('in_admin_header', 'wemcor_hide_notices_to_all', 99);
function wemcor_hide_notices_to_all() {
	//if (!is_super_admin()) {
		remove_all_actions( 'user_admin_notices' );
		remove_all_actions( 'admin_notices' );
	//}
}

//remove nav-menus from customize WordPress
add_action('customize_register', function ( $WP_Customize_Manager ){
    if (isset($WP_Customize_Manager->nav_menus) && is_object($WP_Customize_Manager->nav_menus)) {
        remove_filter( 'customize_refresh_nonces', array( $WP_Customize_Manager->nav_menus, 'filter_nonces' ) );
        remove_action( 'wp_ajax_load-available-menu-items-customizer', array( $WP_Customize_Manager->nav_menus, 'ajax_load_available_items' ) );
        remove_action( 'wp_ajax_search-available-menu-items-customizer', array( $WP_Customize_Manager->nav_menus, 'ajax_search_available_items' ) );
        remove_action( 'customize_controls_enqueue_scripts', array( $WP_Customize_Manager->nav_menus, 'enqueue_scripts' ) );
        remove_action( 'customize_register', array( $WP_Customize_Manager->nav_menus, 'customize_register' ), 11 );
        remove_filter( 'customize_dynamic_setting_args', array( $WP_Customize_Manager->nav_menus, 'filter_dynamic_setting_args' ), 10, 2 );
        remove_filter( 'customize_dynamic_setting_class', array( $WP_Customize_Manager->nav_menus, 'filter_dynamic_setting_class' ), 10, 3 );
        remove_action( 'customize_controls_print_footer_scripts', array( $WP_Customize_Manager->nav_menus, 'print_templates' ) );
        remove_action( 'customize_controls_print_footer_scripts', array( $WP_Customize_Manager->nav_menus, 'available_items_template' ) );
        remove_action( 'customize_preview_init', array( $WP_Customize_Manager->nav_menus, 'customize_preview_init' ) );
        remove_filter( 'customize_dynamic_partial_args', array( $WP_Customize_Manager->nav_menus, 'customize_dynamic_partial_args' ), 10, 2 );

    }
}, -1);

// remove menu items from customize WordPress
add_action( 'customize_register', 'remove_customizer_settings', 20 );
function remove_customizer_settings( $wp_customize ) {
	$blog_id = get_current_blog_id();
	$stylesheet = get_blog_option($blog_id, 'stylesheet');
	/*
	 * Poner condicional para roles y capabilities
	 *
	 */
	//$wp_customize->remove_section( 'title_tagline');
	if( 'twentyten' === $stylesheet)  $wp_customize->remove_section( 'colors');
	//$wp_customize->remove_section( 'header_image');
	//$wp_customize->remove_section( 'background_image');
	//$wp_customize->remove_section( 'static_front_page');
	$wp_customize->remove_section( 'custom_css');

	if( 'generatepress' === $stylesheet) {
		$wp_customize->remove_section( 'generatepress_upsell_section');
	}
}

// add meta tags in header according theme
add_action( 'wp_head', 'wemcor_add_meta_tags' );
function wemcor_add_meta_tags() {
	$blog_id = get_current_blog_id();
	$activate_theme = get_blog_option( $blog_id, 'stylesheet');
	if( 'twentyten' === $activate_theme ) {
		?>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<?php }
}

