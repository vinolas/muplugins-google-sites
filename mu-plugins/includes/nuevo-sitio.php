<?php

if( is_multisite() && current_user_can( 'read' ) ) :
	global $wpdb;

	/** Load WordPress Administration Bootstrap */
	require_once ABSPATH . 'wp-admin/admin.php';

	/** WordPress Translation Installation API */
	require_once ABSPATH . 'wp-admin/includes/translation-install.php';

	if ( isset( $_REQUEST['action'] ) && 'add-site' === $_REQUEST['action'] ) {
		check_admin_referer( 'add-blog', '_wpnonce_add-blog' );

		if ( ! is_array( $_POST['blog'] ) ) {
			wp_die( __( 'Can&#8217;t create an empty site.' ) );
		}

		$blog   = $_POST['blog'];
		$domain = '';

		$blog['domain'] = trim( $blog['domain'] );
		if ( preg_match( '|^([a-zA-Z0-9-])+$|', $blog['domain'] ) ) {
			$domain = strtolower( $blog['domain'] );
		}

		// If not a subdomain installation, make sure the domain isn't a reserved word.
		if ( ! is_subdomain_install() ) {
			$subdirectory_reserved_names = get_subdirectory_reserved_names();

			if ( in_array( $domain, $subdirectory_reserved_names, true ) ) {
				wp_die(
					sprintf(
						/* translators: %s: Reserved names list. */
						__( 'The following words are reserved for use by WordPress functions and cannot be used as blog names: %s', 'wemcor-multisite' ),
						'<code>' . implode( '</code>, <code>', $subdirectory_reserved_names ) . '</code>'
					)
				);
			}
		}

		$title = $blog['title'];
		if( empty( $title ) ) {
			wp_die( __( 'Missing name site.', 'wemcor-multisite' ) );
		}

		$meta = array(
			'public' => 1,
		);

		// Handle translation installation for the new site.
		if ( isset( $_POST['WPLANG'] ) ) {
			if ( '' === $_POST['WPLANG'] ) {
				$meta['WPLANG'] = ''; // en_US
			} elseif ( in_array( $_POST['WPLANG'], get_available_languages(), true ) ) {
				$meta['WPLANG'] = $_POST['WPLANG'];
			} elseif ( current_user_can( 'install_languages' ) && wp_can_install_language_pack() ) {
				$language = wp_download_language_pack( wp_unslash( $_POST['WPLANG'] ) );
				if ( $language ) {
					$meta['WPLANG'] = $language;
				}
			}
		}

		// Forzamos idioma català
		// $meta['WPLANG'] = 'ca';

		if ( empty( $domain ) ) {
			wp_die( __( 'Missing or invalid site address.', 'wemcor-multisite' ) );
		}

		if ( is_subdomain_install() ) {
			$newdomain = $domain . '.' . preg_replace( '|^www\.|', '', get_network()->domain );
			$path      = get_network()->path;
		} else {
			$newdomain = get_network()->domain;
			$path      = get_network()->path . $domain . '/';
		}

		$user = wp_get_current_user();
		$email = $user->user_email;
		if ( isset( $email ) && '' === trim( $email ) ) {
			wp_die( __( 'Missing email address.', 'wemcor-multisite' ) );
		}
		if ( ! is_email( $email ) ) {
			wp_die( __( 'Invalid email address.', 'wemcor-multisite' ) );
		}

		$password = wp_generate_password( 12, false );
		$user_id  = email_exists( $email );

		if ( ! $user_id ) { // Usuario siempre debe existir pues estamos creando desde dentro backend. Solo puede pasar esto si el usuario por alguna razón se manipuló su mail desde base de datos y no dispone de él
			wp_die( __( 'Upps!!! Critical error. Contact with the site administrator', 'wemcor-multisite' ) );
		}



		// super admin id for assign to new blog site
		$super_admin_id = wemcor_get_super_admin();


		$wpdb->hide_errors();
		$id = wpmu_create_blog( $newdomain, $path, $title, $super_admin_id, $meta, get_current_network_id() );
		$wpdb->show_errors();
		if ( ! is_wp_error( $id ) ) {

			// assign user
			if ( ! is_super_admin( $user_id ) && ! get_user_option( 'primary_blog', $user_id ) ) {
				update_user_option( $super_admin_id, 'primary_blog', $id, false );
			}
			$role = wemcor_assign_site($id, $user_id);
			add_user_to_blog($id, $user_id, $role);
			//add_user_to_blog($id, $user_id, 'student');

			// assign owner blog
			update_blog_option( $id, 'owner_user', $user_id );

			// customize settings new site
			switch_to_blog($id);
				$options = [
					'date_format' => 'd/m/Y',
					'time_format' => 'H:i',
					'links_updated_date_format' => 'd/m/Y H:i',
					'timezone_string' => 'Europe/Madrid',
					'template' => (isset($_POST['theme_site'])) ? $_POST['theme_site'] : 'twentytwelve',
					'stylesheet' => (isset($_POST['theme_site'])) ? $_POST['theme_site'] : 'twentytwelve',
					'show_on_front' => 'page'
				];
				wemcor_set_options($options);
				wemcor_delete_all_pages_new_site();
				$post_id = wemcor_create_first_pages();

				//wemcor_create_widgets($options['template']);
				//wemcor_change_role_user($id, $user_id);
				//wpmu_new_site_admin_notification( $id, $user_id );
				//wpmu_welcome_notification( $id, $user_id, $password, $title, array( 'public' => 1 ) );
				//wemcor_asign_new_site_to_users($id, $user_id);
			restore_current_blog();


			//redirect to first new page
			if($post_id) {
				//wp_redirect( get_admin_url($id, 'post-new.php?post_type=page') );//http://test.wemcor.es/prueba1/wp-admin/post-new.php?post_type=page
				wp_redirect( get_admin_url($id, 'post.php?post='.$post_id.'&action=edit') );//https://wp.test.digitaldemocratic.net/tema1/wp-admin/post.php?post=3&action=edit
				exit;
			}
		} else {
			wp_die( $id->get_error_message() );
		}
	}

	?>

	<div class="wrap">
		<h1 id="add-new-site"><?php _e( 'Add new site', 'wemcor-multisite' ); ?></h1>
		<?php
		if ( ! empty( $messages ) ) {
			foreach ( $messages as $msg ) {
				echo '<div id="message" class="updated notice is-dismissible"><p>' . $msg . '</p></div>';
			}
		}
		?>
		<p>
		<?php
		printf(
			/* translators: %s: Asterisk symbol (*). */
			__( 'Required fields are marked %s', 'wemcor-multisite' ),
			'<span class="required">*</span>'
		);
		?>
		</p>

		<form method="post" action="<?php echo admin_url( 'admin.php?page=nuevo-sitio&action=add-site' ); ?>" novalidate="novalidate">

			<?php wp_nonce_field( 'add-blog', '_wpnonce_add-blog' ); ?>

			<table class="form-table" role="presentation">
			<tr class="form-field form-required">
				<th scope="row"><label for="site-address"><?php _e( 'Site Address (URL)', 'wemcor-multisite' ); ?> <span class="required">*</span></label></th>
				<td>
				<?php if ( is_subdomain_install() ) { ?>
					<input name="blog[domain]" type="text" class="regular-text ltr" id="site-address" aria-describedby="site-address-desc" autocapitalize="none" autocorrect="off" required /><span class="no-break">.<?php echo preg_replace( '|^www\.|', '', get_network()->domain ); ?></span>
					<?php
				} else {
					echo get_network()->domain . get_network()->path
					?>
					<input name="blog[domain]" type="text" class="regular-text ltr" id="site-address" aria-describedby="site-address-desc" autocapitalize="none" autocorrect="off" required />
					<?php
				}
				echo '<p class="description" id="site-address-desc">' . __( 'Only lowercase letters (a-z), numbers, and hyphens are allowed.', 'wemcor-multisite' ) . '</p>';
				?>
				</td>
			</tr>
			<tr class="form-field form-required">
				<th scope="row"><label for="site-title"><?php _e( 'Site Title', 'wemcor-multisite' ); ?> <span class="required">*</span></label></th>
				<td><input name="blog[title]" type="text" class="regular-text" id="site-title" required /></td>
			</tr>
			<?php
			$languages    = get_available_languages();
			$translations = wp_get_available_translations();

			if ( ! empty( $languages ) || ! empty( $translations ) ) :
				?>
				<tr class="form-field form-required">
					<th scope="row"><label for="site-language"><?php _e( 'Site Language', 'wemcor-multisite' ); ?></label></th>
					<td>
						<?php
						// Network default.
						//$lang = get_site_option( 'WPLANG' );
						//por defecto siempre será català
						$lang = 'ca';

						// Use English if the default isn't available.
						if ( ! in_array( $lang, $languages, true ) ) {
							$lang = '';
						}

						wp_dropdown_languages(
							array(
								'name'                        => 'WPLANG',
								'id'                          => 'site-language',
								'selected'                    => $lang,
								'languages'                   => $languages,
								'translations'                => $translations,
								'show_available_translations' => current_user_can( 'install_languages' ) && wp_can_install_language_pack(),
							)
						);
						?>
					</td>
				</tr>
			<?php endif; // Languages. ?>
				<!--<tr class="form-field">
					<td colspan="2" class="td-full"><p id="site-admin-email"><?php //_e( 'A new user will be created if the above email address is not in the database.', 'wemcor-multisite' ); ?><br /><?php //_e( 'The username and a link to set the password will be mailed to this email address.' ); ?></p></td>
				</tr>-->
				<tr class="form-field form-required">
					<td><input id="theme_site" type="hidden" name="theme_site" value="twentytwelve"></td>
				</tr>
			</table>

			<div class="section-title-choose-theme">
				<p class="label-choose-theme"><?php esc_html_e('Choose Theme', 'wemcor-multisite'); ?></p><small><?php esc_html_e('Default theme: Theme 1', 'wemcor-multisite'); ?></small>
			</div>
			<div id="images_theme">
			<?php
			$files = list_files( dirname(__FILE__) . '/../screenshots/', 1);
			$i = 0;
			sort($files);
			foreach( $files as $file ) {
				$name = explode("/", $file);
				$name = end($name);//1-twentytwelve.png
				$explode_name = explode('-', $name);
				$end_name = str_replace('.png', '', $explode_name[1]);//twentywelve
				//$description = wp_get_theme($end_name)['description'];

				if($i) $style = 'display:none;';
				else $style = '';
				$i++;

				?>

				<div class="item-image">
					<div class="item-image-wrap">
						<img id="<?php echo $end_name; ?>" class="image-theme" width="220" src="<?php echo plugin_dir_url(dirname(__FILE__)) ?>screenshots/<?php echo $name; ?>"><span class="tick" style="<?php echo $style; ?>">&check;</span>
					</div>
					<div class="item-content-wrap">
						<h4><?php esc_html_e('Theme '.$i, 'wemcor-multisite'); ?></h4>
						<p><?php esc_html_e('Description Theme '.$i, 'wemcor-multisite'); ?></p>

					</div>
				</div>
			<?php } ?>
			</div>
			<div style="clear:both;"></div>

			<?php submit_button( __( 'Add new site', 'wemcor-multisite' ), 'primary', 'add-site' );
			?>
		</form>

	</div><!-- end wrap -->

<?php

endif;



function wemcor_asign_new_site_to_users( $id, $user_id ) {

	//get users by role manager / administrator
	$args = array(
	    'role__in'     => array('manager', 'administrator'),
	    'role__not_in' => array('teacher', 'student'),
	    'blog_id'      => $id
	);
	$users = get_users( $args );
	foreach( $users as $user ) {
		add_user_to_blog( $id, $user_id, 'manager' );
	}

}

function wemcor_change_role_user($id, $user_id) {
	$user = new WP_User( $user_id );
	// Roles
	$roles = $user->roles;
	// Remove role. Creado blog nuevo con solo un usuario que es administrador
	// remove_user_from_blog($user_id , $id);
	// Add role. asignamos el mismo usuario al mismo blog pero con role student o teacher (según el que sea cuando ha creado el blog)

	if( in_array('manager', $roles) ) add_user_to_blog( $id, $user_id, 'manager' );
	else if( in_array('teacher', $roles) ) add_user_to_blog( $id, $user_id, 'teacher' );
	else if( in_array('student', $roles) ) add_user_to_blog( $id, $user_id, 'student' );

}


function wemcor_set_options($options) {
	foreach( $options as $option => $value ) {
		update_blog_option($id, $option, $value);
	}
}

function wemcor_delete_all_pages_new_site() {
		$page_ids = get_all_page_ids();
		foreach( $page_ids as $page_id ) {
			wp_delete_post( $page_id, true );
		}
}

function wemcor_create_first_pages() {
	$postarr = [
		'post_title' => __('Pàgina d\'exemple', 'wemcor-multisite'),
		'post_content' => '<!-- wp:generateblocks/container {"uniqueId":"a4c6887b","containerWidth":960,"paddingTop":"180","paddingRight":"100","paddingBottom":"180","paddingLeft":"100","paddingTopTablet":"120","paddingBottomTablet":"120","paddingTopMobile":"60","paddingRightMobile":"20","paddingBottomMobile":"60","paddingLeftMobile":"20","backgroundColor":"#226e93","gradient":true,"gradientDirection":0,"gradientColorOne":"#242424","gradientColorOneOpacity":0.4,"gradientColorStopOne":0,"gradientColorTwo":"#242424","gradientColorTwoOpacity":0.4,"gradientColorStopTwo":100,"bgImage":{"id":143,"image":{"url":"%%DOMAIN%%/encabezamiento-1.png","height":544,"width":725,"orientation":"landscape"}},"bgOptions":{"selector":"pseudo-element","opacity":1,"overlay":false,"position":"center center","size":"cover","repeat":"no-repeat","attachment":""},"showAdvancedTypography":true,"align":"full","isDynamic":true} -->
<!-- wp:heading {"textAlign":"center","level":1,"style":{"typography":{"fontSize":80}},"textColor":"white"} -->
<h1 class="has-text-align-center has-white-color has-text-color" style="font-size:80px">Title page</h1>
<!-- /wp:heading -->
<!-- /wp:generateblocks/container -->

<!-- wp:paragraph -->
<p>Start writing the content of the page here. You can include images and links</p>
<!-- /wp:paragraph -->',
		'post_status' => 'publish',
		'post_type' => 'page'
	];

	$postarr['post_content'] = str_replace('%%DOMAIN%%', 'https://'.$_ENV["WORDPRESS_DOMAIN_CURRENT_SITE"].'/wp-content/mu-plugins/images', $postarr['post_content']);

	$page_on_front = wp_insert_post($postarr, false, false);
	update_option('page_on_front', $page_on_front);

	//create page for blog
	unset($postarr['content']);
	$postarr['post_title'] = __('Blog', 'wemcor-multisite');
	$page_for_posts = wp_insert_post($postarr, false, false);
	update_option('page_for_posts', $page_for_posts);

	return $page_on_front;
}

function wemcor_get_super_admin() {
	$super_admin_ids = get_super_admins();
	$super_admin = get_user_by('login', $super_admin_ids[0]);
	$super_admin_id = $the_user->ID;

	return $super_admin_id;
}

function wemcor_assign_site($id, $user_id) {
	$user_meta = get_userdata($user_id);
	$user_roles = $user_meta->roles;
	if ( in_array( 'manager', $user_roles ) ) return 'manager';
	if ( in_array( 'teacher', $user_roles ) ) return 'teacher';
	if ( in_array( 'student', $user_roles ) ) return 'student';

	return 'subscriber';
}
