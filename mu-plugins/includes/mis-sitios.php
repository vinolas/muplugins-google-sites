<?php

if( is_multisite() && current_user_can( 'read' ) ) :
	$user = wp_get_current_user();

	$user_id = get_current_user_id();

	$title = 'Els meus llocs webs';

	$blogs = get_blogs_of_user( $user_id );

	?>

	<div class="wrap">
		<h1 class="wp-heading-inline">
		<?php
		echo __('My websites', 'wemcor-multisite');
		?>
		</h1>

		<form id="myblogs" method="post">
			<ul class="my-sites striped">
				<?php
				//reset( $blogs );
				foreach ( $blogs as $user_blog ) {
					switch_to_blog( $user_blog->userblog_id );

					$src = get_screenshot_stylesheet(get_option('stylesheet'));

					echo '<li class="mi-sitio">';

						//image
						echo '<div class="item-mi-sitio-image">';
							if($src) echo '<img style="width:100%;height:auto;" src="'.$src.'"/>';
							else echo '<img style="width:100%;height:auto;" alt="screenshot" src="http://s.wordpress.com/mshots/v1/'.urlencode(home_url()).'"/>';
						echo '</div>';

						//content
						echo '<div class="item-mi-sitio-content">';

							//title + actions
							echo '<div class="item-mi-sitio-title">';
								echo "<h3>{$user_blog->blogname}</h3>";
								$actions = "<a href='" . esc_url( home_url() ) . "'>" . __( 'Visit web', 'wemcor-multisite' ) . '</a>';

								if ( current_user_can( 'read' ) ) {
									$actions .= "<a href='" . esc_url( admin_url() ) . "edit.php?post_type=page'>" . __( 'All pages', 'wemcor-multisite' ) . '</a>';
									// $actions .= "<a href='" . esc_url( admin_url() ) . "index.php'>" . __( 'View Dashboard', 'wemcor-multisite' ) . '</a>';
									// $actions .= "<a href='" . esc_url( admin_url() ) . "admin.php?page=nuevo-sitio'>" . __( 'Afegir nou lloc web', 'wemcor-multisite' ) . '</a>';
								}

								if ( in_array( 'teacher', $user->roles) || in_array('manager', $user->roles) || in_array('administrator', $user->roles) ) {
								//if ( current_user_can( 'manage_sites' ) ) {

									if( BLOG_ID_CURRENT_SITE != $user_blog->userblog_id ) {
										$publish_web = get_blog_option( $user_blog->userblog_id, 'wemcor-publishweb' );
										if($publish_web) {
											$actions .= "<a href='" . esc_url( admin_url() ) . "admin.php?page=publish-web'>" . __( 'Unpublish web', 'wemcor-multisite' ) . '</a>';//https://wp.test.digitaldemocratic.net/wp-admin/admin.php?page=publish-web
										} else {
											$actions .= "<a href='" . esc_url( admin_url() ) . "admin.php?page=publish-web'>" . __( 'Publish web', 'wemcor-multisite' ) . '</a>';//https://wp.test.digitaldemocratic.net/wp-admin/admin.php?page=publish-web
										}
									}

									$actions .= "<a href='" . esc_url( admin_url() ) . "admin.php?page=assign-users'>" . __( 'Assign Users', 'wemcor-multisite' ) . '</a>';//https://wp.test.digitaldemocratic.net/wp-admin/admin.php?page=assign-users
								}

								echo '<p class="my-sites-actions">' . $actions . '</p>';
							echo '</div>';

							//pages
							$pages = get_pages(['post_status'=>'publish']);
							if($pages) {
								echo '<div class="item-pages">';
									echo '<p style="font-weight:bold;">'. __('Website Pages', 'wemcor-multisite').'</p>';
									foreach($pages as $page) {
										$owner = wemcor_get_owner($user_blog->userblog_id);
										echo '<p>'.$page->post_title.' <a href="'.home_url($page->post_name).'">'. __('Visit', 'wemcor-multisite') .'</a>';
										if($owner || get_current_user_id() == 1) echo '| <a href="'.admin_url().'post.php?post='.$page->ID.'&action=edit">'. __('Edit', 'wemcor-multisite') .'</a>';
										echo '</p>';
									}
								echo '</div>';
							}

						echo '</div>';
					echo '</li>';

					restore_current_blog();
				}
				?>
			</ul>
		</form>

		<?php
		if ( current_user_can( 'read' ) ) echo '<p style="margin-top: 40px;"><a href="'.admin_url().'admin.php?page=nuevo-sitio" class="button button-primary">'. __('Add new website', 'wemcor-multisite') .'</a></p>';
		?>
	</div>

<?php
endif;

function get_screenshot_stylesheet($stylesheet) {
	switch ($stylesheet) {
		case 'twentyten':
			$index = '1';
			break;
		case 'twentytwelve':
			$index = '2';
			break;
		case 'generatepress':
			$index = '3';
			break;
		default:
			return false;
	}

	return 'https://'.$_ENV["WORDPRESS_DOMAIN_CURRENT_SITE"].'/wp-content/mu-plugins/screenshots/'.$index.'-'.$stylesheet.'.png';
}
