<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Wemcor_Users {

	static $instance;

	public $users_obj;

	public function __construct() {
		//add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'add_menu_assign_users' ] );
	}

	// public static function set_screen( $status, $option, $value ) {
	// 	return $value;
	// }

	public function add_menu_assign_users() {
		//$role = wemcor_get_role_user();
		//if( $role == 'student' ) return;

		$hook = add_menu_page(
			__('Assign Users', 'wemcor-multisite'),
			__('Assign Users', 'wemcor-multisite'),
			'read',
			'assign-users',
			[$this, 'assign_users_callback'],
			'dashicons-superhero-alt',
			4
		);

		add_action( "load-$hook", [$this, 'screen_options'] );
		//$this->users_obj = new Users_List();
	}

	public function screen_options() {
		$option = 'per_page';
		$args   = [
			'label'   => __('Assign Users', 'wemcor-multisite'),
			'default' => 20,
			'option'  => 'users_per_page'
		];
		$option = '';
		$args = [];

		add_screen_option( $option, $args );
		$this->users_obj = new Users_List();
	}

	public function assign_users_callback() {
		$blog_id = get_current_blog_id();
		$blog_details = get_blog_details($blog_id);
		?>
		<div id="wemcor-wrap" class="wrap wemcor-table-users">
			<h1><?php esc_html_e('Assign Users', 'wemcor-multisite'); ?></h1>
			<h2><?php esc_html_e('Site:', 'wemcor-multisite'); ?><strong>&nbsp;<?php echo $blog_details->blogname; ?></strong></h2>
			<div id="poststuff">
				<div id="post-body" class="metabox-holder">
					<div id="post-body-content">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">
								<?php
								if(isset($_POST['s'])) $this->users_obj->prepare_items($_POST['s']);
								else $this->users_obj->prepare_items();
								$this->users_obj->views();
								$this->users_obj->search_box(__('Search', 'wemcor-multisite'), 'search');
								$this->users_obj->display();
								?>
							</form>
						</div>
					</div>
				</div>
				<br class="clear">
			</div>
		</div>
	<?php
	}

	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

}


class Users_List extends WP_List_Table {

	public function __construct() {

		parent::__construct( [
			'singular' => __( 'Assign User', 'wemcor-multisite' ),
			'plural'   => __( 'Assign Users', 'wemcor-multisite' ),
			'ajax'     => false
		] );

	}

	public function get_columns() {
	  	$columns = [
		    'id'      			=> __( 'Id', 'wemcor-multisite'),
		    'first'				=> __( 'First name', 'wemcor-multisite'),
		    'last'    			=> __( 'Last name', 'wemcor-multisite' ),
		    'email'		        => __( 'Email', 'wemcor-multisite'),
		    'role'				=> __( 'Role', 'wemcor-multisite' ),
		    'groups'		    => __( 'Groups', 'wemcor-multisite' ),
		    'assign'      		=> __( 'Assign site', 'wemcor-multisite' )
		    //'cb'      			=> '<input type="checkbox" />',
	  	];

	  	return $columns;

	}

	public function process_bulk_action() {
		if ( 'assign' === $this->current_action() ) {
			$nonce = esc_attr( $_REQUEST['_wpnonce'] );
			if ( ! wp_verify_nonce( $nonce, 'wemcor_assign_nonce' ) ) {
	   			die( 'Error' );
	 		}

			self::assign_user( absint( $_GET['assign'] ) );
			$url = admin_url( 'admin.php?page=assign-users', 'https' );
			$message = '<h2>'.__('Assign users sucesfully', 'wemcor-multisite').'</h2><br><a href="'.$url.'">'.__('<< Back to page', 'wemcor-multisite').'</a>';
	   		wp_die( $message );
		}

		//bulk actions
		if ( isset( $_POST['action']) || isset( $_POST['action2']) ) {
		 	if ( (isset($_POST['action']) && $_POST['action'] == 'bulk-assign' ) || (isset( $_POST['action2']) && $_POST['action2'] == 'bulk-assign' ) ) {
			 	$users_ids = esc_sql( $_POST['bulk-assign'] );
			 	self::assign_users_bulk($users_ids);
			 	$message = '<h2>'.__('Assign users sucesfully', 'wemcor-multisite').'</h2><br><a href="'.$url.'">'.__('<< Back to page', 'wemcor-multisite').'</a>';
	   			wp_die( $message );
		   	}
		}

	}

	// public function get_bulk_actions() {
	// 	return ['bulk-assign'  => __('Assign users to site', 'wemcor-multisite')];
	// }

	protected function get_views() {
		$views = array();
		$current = ( !empty($_REQUEST['customvar']) ? $_REQUEST['customvar'] : 'all');

		//Alls
		$all = __('All', 'wemcor-multisite');
		$class = ($current == 'all' ? ' class="current"' :'');
		$all_url = remove_query_arg('customvar');
		$views['all'] = "<a href='{$all_url }' {$class} >{$all}</a>";

		//Assigned
		$assigned = __('Assigned', 'wemcor-multisite');
		$foo_url = add_query_arg('customvar','assigned');
		$class = ($current == 'assigned' ? ' class="current"' :'');
		$views['assigned'] = "<a href='{$foo_url}' {$class} >{$assigned}</a>";

		//Unassigned
		$unassigned = __('Unassigned', 'wemcor-multisite');
		$bar_url = add_query_arg('customvar','unassigned');
		$class = ($current == 'unassigned' ? ' class="current"' :'');
		$views['unassigned'] = "<a href='{$bar_url}' {$class} >{$unassigned}</a>";

		return $views;
	}

	public static function get_users($per_page = 20, $page_number = 1, $search = '') {
		$total_items  = self::record_count();
		$text = __('at ', 'wemcor-multisite'). current_time('d-m-Y H:i');
		echo ($search != '') ? '<h4>'.__('Search users for', 'wemcor-multisite').' '.$search.' '.$text.'</h4>' : '<h4>'.$total_items.' '.__('users', 'wemcor-multisite').' '.$text.'</h4>';

		//el buscador busca en id, en email, en el first, en el last, en todos lados
		if( $search == '') {
			$url = 'http://isard-sso-admin:9000/api/internal/users';
			$users = json_decode(file_get_contents($url), true);
		} else {
			$url = 'http://isard-sso-admin:9000/api/internal/users/filter';
			$data = [
	    		'text' => $search,
			];
			$users_txt = self::CallAPI("POST", $url, $data);
			$users = json_decode($users_txt, true);
		}

		//si existe filtro de roles quitar del json generado ($users) los roles que no correspondan
		if( isset($_POST['role-filter']) && !empty($_POST['role-filter']) ) {
			$users = self::map_roles($users, $_POST['role-filter']);
		}

		//si existe get assigned / unassigned eliminamos del json generado los usuarios que no correspondan
		if( isset($_GET['customvar']) ) $users = self::map_assigneds($users, $_GET['customvar']);

		$users = self::unset_current_user($users);

		return $users;
	}

	public static function map_assigneds( $users, $get_assigned ) {

		foreach( $users as $key => $user ) {
			//comprobar si usuario existe
			$checked = self::checked_assign_user($user['id']);
			if( $get_assigned == 'assigned' && !$checked ) unset($users[$key]);
			if( $get_assigned == 'unassigned' && $checked ) unset($users[$key]);
		}

		return $users;
	}

	public static function get_groups() {
		$url = 'http://isard-sso-admin:9000/api/internal/groups';
		$groups = json_decode(file_get_contents($url), true);

		return $groups;
	}

	public static function get_roles() {
		$url = 'http://isard-sso-admin:9000/api/internal/roles';
		$roles = json_decode(file_get_contents($url), true);

		return $roles;
	}

	public static function get_groups_users($per_page = 20, $page_number = 1, $search = '') {
		// Method: POST, PUT, GET etc
		// Data: array("param" => "value") ==> index.php?param=value

		$url = 'http://isard-sso-admin:9000/api/internal/group/users';
		$data = [
	    	"path" => $_POST['group-filter'],
	    	"text" => $search
		];
		$users_txt = self::CallAPI("POST", $url, $data);
		$users = json_decode($users_txt, true);

		//si existe filtro de roles quitar del json generado ($users) los roles que no correspondan
		if( isset($_POST['role-filter']) && !empty($_POST['role-filter']) ) {
			$users = self::map_roles($users, $_POST['role-filter']);
		}

		//si existe get assigned / unassigned eliminamos del json generado los usuarios que no correspondan
		if( isset($_GET['customvar']) ) $users = self::map_assigneds($users, $_GET['customvar']);

		return $users;

	}

	public static function unset_current_user($users) {
		$current_user = wp_get_current_user();
		foreach ($users as $key => $user) {
// print_r($user['id']);
// print_r($current_user->user_login);
			if( $user['id'] == $current_user->user_login ) unset($users[$key]);
		}

		return $users;
	}

	public static function map_roles($users, $post_role) {
		// manager = 67cd7110-0433-42eb-92b6-e6f5d2c9f320
		// student = 7f7c2caf-40dc-4529-ada6-f2cd70998ebb
		// teacher = e1bc6519-8ab2-4956-9f4e-423480fce8c8
		switch($post_role) {
			case '67cd7110-0433-42eb-92b6-e6f5d2c9f320': $role = 'manager';
			break;
			case '7f7c2caf-40dc-4529-ada6-f2cd70998ebb': $role = 'student';
			break;
			case 'e1bc6519-8ab2-4956-9f4e-423480fce8c8': $role = 'teacher';
			break;
		}
		foreach ($users as $key => $user) {
			if( $user['role'] != $role ) unset($users[$key]);
		}

		return $users;
	}

	public static function assign_users() {
		$blog_id = get_current_blog_id();

		if( isset($_POST['bulk-assign']) && !empty($_POST['bulk-assign']) ) {
			$user_ids = $_POST['bulk-assign'];
			$user_assigneds = [];
			foreach( $user_ids as $user_id ) {
				//comprobar si usuario existe
				$user = get_user_by( 'login', $user_id );
				$password = wp_generate_password( 12, true, true );
				if( $user ) $id_user = $user->ID;
				else $id_user = wp_create_user( $user_id, $password, '' );
				//leer role del usuario
				$url = 'http://isard-sso-admin:9000/api/internal/users/filter';
				$data = [
		    		'text' => $user_id,
				];
				$users_txt = self::CallAPI("POST", $url, $data);
				$users = json_decode($users_txt, true);
				foreach( $users as $user ) {
					if($user['id'] == $user_id) $user_role = $user['role'];
					break;
				}

				// prevent default
				if( !isset($user_role) ) $user_role = 'student';

				//añadir a blog
				add_user_to_blog($blog_id, $id_user, $user_role);
				$users_assigneds[] = $id_user;
			}

			//array de asignados
			$blog_users = get_users( ['blog_id' => $blog_id]);
			foreach( $blog_users as $blog_user ) {
				if($blog_user->ID == 1 || $blog_user->ID == get_current_user_id() ) continue;

				//temporal (tests)
				if( $blog_user->ID == 11 || $blog_user->ID == 12 || $blog_user->ID == 13 || $blog_user->ID == 14) continue;

				if( in_array($blog_user->data->ID, $users_assigneds) ) continue;
				remove_user_from_blog( $blog_user->ID, $blog_id );
			}
		} else {
			//if( $blog_id == BLOG_ID_CURRENT_SITE ) return;

			$blog_users = get_users( ['blog_id' => $blog_id]);
			foreach( $blog_users as $blog_user ) {
				if($blog_user->ID == 1 || $blog_user->ID == get_current_user_id() ) continue;

				//temporal (tests)
				if( $blog_user->ID == 11 || $blog_user->ID == 12 || $blog_user->ID == 13 || $blog_user->ID == 14) continue;

				remove_user_from_blog( $blog_user->ID, $blog_id );
			}
		}
	}

	public static function CallAPI( $method, $url, $data = false ) {
		$curl = curl_init();

		switch ($method) {
		    case "POST":
		        curl_setopt($curl, CURLOPT_POST, 1);
		        if ($data) {
		            $payload = json_encode( $data );
		            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		            curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
		        }
		        break;
		    //case "PUT":
		        //curl_setopt($curl, CURLOPT_PUT, 1);
		        //break;
		    //default:
		        //if ($data) $url = sprintf( "%s?%s", $url, http_build_query($data) );
		}

		// Optional Authentication:
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_USERPWD, "username:password");

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($curl);

		curl_close($curl);

		return $result;
	}

	public static function show_users( $users ) {
		foreach ($users as $user ) {
			foreach( $user as $key => $value ) {
				if( ! is_array($user[$key]) ) {
					echo $key.': ' . $value . '<br>';
				} else {
					echo $key .': <ul>';
					foreach($user[$key] as $group) {
						echo '<li>'. $group .'</li>';
					}
					echo '</ul>';
				}
			}
			echo '<hr>';
		}
	}

	public static function record_count() {
		$url = 'http://isard-sso-admin:9000/api/internal/users';
		$users = json_decode(file_get_contents($url), true);

		return count($users);
	}

	public function no_items() {
		_e( 'Not found users', 'wemcor-multisite' );
	}

	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'id':
		    case 'first':
		    case 'last':
		    case 'email':
		    case 'role':
		    case 'groups':
		    case 'assign':
	      		return $item[ $column_name ];
	      		break;
	  		default:
	  			return '-';
		}
	}

	public function column_assign( $item ) {
		//nonces
		$assign_nonce = wp_create_nonce( 'wemcor_assign_nonce' );
		//buscar si usuario existe en el blog
		$checked = self::checked_assign_user($item['id']);
		if( $checked ) return sprintf('<input type="checkbox" name="bulk-assign[]" value="%s" checked/>', $item['id']);

		return sprintf('<input type="checkbox" name="bulk-assign[]" value="%s" />', $item['id']);
	}

	public static function checked_assign_user($id) {
		$blog_id = get_current_blog_id();
		$user = get_user_by( 'login', $id );
		if( $user ) return is_user_member_of_blog( $user->ID, $blog_id );

		return false;
	}

	public function column_groups( $item ) {
		if( is_array($item['groups']) ) return implode(', ', $item['groups']);
		return $item['groups'];
	}

	// public function get_sortable_columns() {
	// 	$sortable_columns = array(
	// 		'id'	  => ['id', true],
	// 		'first'	  => ['first', true],
	// 		'last'	  => ['last', true],
	//     	'email'	  => ['email', true],
	//     	'role'    => ['role', true],
	//     	'assign'  => ['assign', true]
	// 	);

	// 	return $sortable_columns;
	// }

	public function extra_tablenav( $which ) {
		$groups = self::get_groups();
		$roles = self::get_roles();
		if ( $which == "top" ) { ?>
		<div style="margin-bottom:20px;float:left;">
			<p class="wemcor-legend" style="margin-bottom:20px;">
				<?php
				esc_html_e('Check to assign user to this website. Uncheck the checkbox for a user to stop belonging to a site. The user is not deleted, they just no longer belong to the website. You can filter by groups or roles to find users more easily. Use the search engine to find users who match your search with their first name, last name or email address.', 'wemcor-multisite');
				?>
			</p>
			<label class="screen-reader-text" for="group"><?php esc_html_e('Filter by group', 'wemcor-multisite'); ?></label>
		    <select name="group-filter" id="group" class="postform">
	            <option value=""><?php esc_html_e('Filter by group', 'wemcor-multisite'); ?></option>
                <?php
                foreach( $groups as $group ) {
                    $selected = '';
                    if( isset($_POST['group-filter']) && $_POST['group-filter'] == $group['id'] ){
                        $selected = ' selected = "selected"';
                    }
                    ?>
                <option value="<?php echo $group['id']; ?>" <?php echo $selected; ?>><?php echo $group['name']; ?></option>
                <?php
            	} ?>
		    </select>
		    <label class="screen-reader-text" for="group"><?php esc_html_e('Filter by role', 'wemcor-multisite'); ?></label>
		    <select name="role-filter" id="group" class="postform">
	            <option value=""><?php esc_html_e('Filter by role', 'wemcor-multisite'); ?></option>
                <?php
                foreach( $roles as $role ) {
                    $selected = '';
                    if( isset($_POST['role-filter']) && $_POST['role-filter'] == $role['id'] ){
                        $selected = ' selected = "selected"';
                    }
                    ?>
                <option value="<?php echo $role['id']; ?>" <?php echo $selected; ?>><?php echo $role['name']; ?></option>
                <?php
            	} ?>
		    </select>
		    <?php
		    submit_button( __( 'Filter', 'wemcor-multisite' ), '', 'filter_action', false, array( 'id' => 'post-query-submit' ) );
		    submit_button( __( 'Apply changes', 'wemcor-multisite' ), 'primary alignright', 'filter_action_assign', false, array( 'id' => 'post-query-submit-assign' ) ); ?>
		</div>
	    <?php }

	   	if ( $which == "bottom" ) {
	   		// code for below table
	   		submit_button( __( 'Apply changes', 'wemcor-multisite' ), 'primary alignright', 'filter_action_assign_2', false, array( 'id' => 'post-query-submit-assign' ) );
	   	}
	}

	public function prepare_items($search = '') {

		$this->_column_headers = $this->get_column_info();

		$this->process_bulk_action();

		$per_page     = $this->get_items_per_page( 'users_per_page' );
		$current_page = $this->get_pagenum();
		//$total_items  = self::record_count();

		/*$this->set_pagination_args( [
	    	'total_items' => $total_items,
	    	'per_page'    => $per_page
		] );*/

		// if( ( isset($_POST['filter_action_assign']) || isset($_POST['filter_action_assign_2']) ) && isset($_POST['bulk-assign']) && !empty($_POST['bulk-assign']) ) self::assign_users( $_POST['bulk-assign'] );

		if( isset($_POST['filter_action_assign']) || isset($_POST['filter_action_assign_2']) ) self::assign_users();

		if( isset($_POST['group-filter']) && !empty($_POST['group-filter']) ) $this->items = self::get_groups_users( $per_page, $current_page, $search );
		else $this->items = self::get_users( $per_page, $current_page, $search );


	}

}