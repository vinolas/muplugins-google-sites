<?php
/*
Plugin Name: Wemcor Roles Multisite
Plugin URI:
Description: Definición de roles y permisos personalizados. Anulación de roles de WordPress. Compatible con multisite
Author: Wemcor
Author URI: https://wemcor.com
Version: 2.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*
 * Roles personalizados
 *
 * Creamos dos roles que tendrán diferentes permisos
 * Rol Teacher podrá publicar sitios y crearlos
 * Rol Student podrá crear sitios pero no publicarlos
 * Ninguno de estos dos roles podrá hacer otro tipo de tareas propias de administrador como activar / editar temas y plugins
 *
 */
//add_action( 'init', 'wemcor_add_custom_roles', 99 );
// function wemcor_add_custom_roles() {
// 	//Default capabilities for Teacher
// 	$capabilities = [
// 		//pages
// 		'delete_others_pages' => true,
// 		'delete_pages' => true,
// 		'delete_private_pages' => true,
// 		'delete_publisehd_pages' => true,
// 		'edit_others_pages' => true,
// 		'edit_pages' => true,
// 		'edit_private_pages' => true,
// 		'edit_published_pages' => true,
// 		'publish_pages' => true,
// 		'read_private_pages' => true,
// 		//posts
// 		'delete_others_posts' => true,
// 		'delete_posts' => true,
// 		'delete_private_posts' => true,
// 		'delete_publisehd_posts' => true,
// 		'edit_others_posts' => true,
// 		'edit_posts' => true,
// 		'edit_private_posts' => true,
// 		'edit_published_posts' => true,
// 		'publish_posts' => true,
// 		'read_private_posts' => true,
// 		//themes
// 		'delete_themes' => false,
// 		'edit_theme_options' => false,
// 		'edit_themes' => false,
// 		'install_themes' => false,
// 		'switch_themes' => false,
// 		'update_themes' => false,
// 		//plugins
// 		'activate_plugins' => false,
// 		'delete_plugins' => false,
// 		'edit_plugins' => false,
// 		'install_plugins' => false,
// 		'update_plugins' => false,
// 		//users
// 		'add_users' => true,
// 		'create_users' => false,
// 		'delete_users' => false,
// 		'edit_users' => true,
// 		'list_users' => true,
// 		'promote_users' => true,
// 		'remove_users' => false,
// 		//core & others & general
// 		'customize' => false,
// 		'edit_dashboard' => false,
// 		'edit_files' => true,
// 		'export' => false,
// 		'import' => false,
// 		'manage_categories' => true,
// 		'manage_links' => true,
// 		'manage_options' => false,
// 		'moderate_comments' => true,
// 		'read' => true,
// 		'unfiltered_html' => true,
// 		'update_core' => false,
// 		'upload_files' => true,
// 	];

// 	//capabilities de multisite
// 	if ( is_multisite() ) :
// 		//sites (multisite)
// 		$capabilities['create_sites'] = true;
// 		$capabilities['delete_sites'] = true;
// 		$capabilities['manage_network'] = true;
// 		$capabilities['manage_network_options'] = true;
// 		$capabilities['manage_network_plugins'] = false;
// 		$capabilities['manage_network_themes'] = false;
// 		$capabilities['manage_network_users'] = true;
// 		$capabilities['manage_sites'] = true;
// 		$capabilities['setup_network'] = true;
// 		$capabilities['upgrade_network'] = false;
// 	endif;

// 	/*
// 	 * Teacher
// 	 */
// 	if ( is_multisite() ) add_role( 'teacher', __('Teacher', 'wemcor-multisite'), $capabilities );

// 	/*
// 	 * Student
// 	 */
// 	//modificamos algunas de las capabilities de los argumentos para crear student
// 	if ( is_multisite() ) $capabilities['manage_network_users'] = false;
// 	//if ( is_multisite() ) $capabilities['manage_sites'] = false;
// 	$capabilities['add_users'] = false;
// 	$capabilities['edit_users'] = false;
// 	$capabilities['list_users'] = false;
// 	$capabilities['promote_users'] = false;
// 	$capabilities['moderate_comments'] = false;
// 	if ( is_multisite() ) add_role( 'student', __('Student', 'wemcor-multisite'), $capabilities );

// 	/*
// 	 * Manager
// 	 */
// 	//creamos manager poniendo todas las capblities a true excepto las del array no_caps
// 	if ( is_multisite() ) {
// 		$no_caps = array(
// 			'manage_network_plugins',
// 			'manage_network_themes',
// 			'upgrade_network',
// 			'delete_themes',
// 			'edit_theme_options',
// 			'edit_themes',
// 			'install_themes',
// 			'switch_themes',
// 			'update_themes',
// 			'activate_plugins',
// 			'delete_plugins',
// 			'edit_plugins',
// 			'install_plugins',
// 			'update_plugins'
// 		);
// 		foreach( $capabilities as $key => $value ) {
// 			if( in_array( $key, $no_caps) ) $capabilities[$key] = false;
// 			else $capabilities[$key] = true;
// 		}
// 		add_role( 'manager', __('Manager', 'wemcor-multisite'), $capabilities );
// 	}
// 	$manager = get_role('manager');
// }

/*
 * Eliminar Roles
 *
 * Eliminar Roles Multisite / WordPress excepto superadmin y los creados Teacher y Student
 *
 */
add_action( 'init', 'wemcor_remove_roles', 10 );
function wemcor_remove_roles() {
	if( is_multisite() ) :
		//Admin
		remove_role( 'admin' );
		//Editor
		remove_role( 'editor' );
		//Author
		remove_role( 'author' );
		//Contributor
		remove_role( 'contributor' );
		//Subscriber
		remove_role( 'subscriber' );
		/*anular cuando esté testeado*/
		//Teacher
		remove_role( 'teacher' );
		//Student
		remove_role( 'student' );
		//Manager
		remove_role( 'manager' );
	endif;
}

/*
 * Clone roles administrartor & delete capabilities
 */
add_action('init', 'wemcor_clone_role_administrator', 99);
function wemcor_clone_role_administrator() {
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $adm = $wp_roles->get_role('administrator');
    $wp_roles->add_role('teacher', __('Teacher', 'wemcor-multisite'), $adm->capabilities);
    $wp_roles->add_role('student', __('Student', 'wemcor-multisite'), $adm->capabilities);
    $wp_roles->add_role('manager', __('Manager', 'wemcor-multisite'), $adm->capabilities);

    wemcor_delete_capabilities('teacher');
    wemcor_delete_capabilities('student');
    wemcor_delete_capabilities('manager');

}

function wemcor_delete_capabilities($role) {
    $role_caps = get_role( $role );
	$caps = array(
		'manage_network_plugins',
		'manage_network_themes',
		'upgrade_network',
		'delete_themes',
		//'edit_theme_options',
		'edit_themes',
		'install_themes',
		'switch_themes',
		'update_themes',
		'activate_plugins',
		'delete_plugins',
		'edit_plugins',
		'install_plugins',
		'update_plugins'
	);
	foreach ( $caps as $cap ) {
	    $role_caps->remove_cap( $cap );
	}
}



// mostramos menu según roles del usuario conectado
add_action( 'admin_menu', 'wemcor_hide_menu', 999 );
function wemcor_hide_menu() {
	// echo '<pre>';
	// var_dump($GLOBALS['menu']);
	// echo '</pre>';

	if( is_admin() ) {
		$role = wemcor_get_role_user();

		/*
		 * Refactorizar de forma que sea al revés. Que lea solo los permitidos y asi se evita que cuando se active un nuevo plugin se muestre en el menu
		 *
		 * Hay veces que no borra bien el menu (!!!!!!REVISAR)
		 */

		$allowed = [
			'edit.php',
			'upload.php',
			'profile.php',
			'mis-sitios',
			'nuevo-sitio',
			'admin.php',
			'post-new.php',
			'edit-tags.php',
			'media-new.php',
			'customize.php'
		];

		if( $role == 'student' || $role == 'teacher' ) {
			remove_menu_page('options-general.php');
			remove_menu_page('tools.php');
			remove_menu_page('edit-comments.php');
			remove_menu_page('generateblocks');
			remove_menu_page('loco');//
			remove_menu_page('wpda');//
			remove_submenu_page( 'users.php', 'user-new.php' );
        	remove_submenu_page( 'users.php', 'users.php' );
        	remove_submenu_page( 'themes.php', 'themes.php');
			remove_submenu_page( 'themes.php', 'generate-options');
			remove_submenu_page( 'themes.php', 'widgets.php');
			remove_submenu_page( 'themes.php', 'nav-menus.php');
		}

		if( $role == 'student' ) {
			$allowed[] = 'publish-web';
			$allowed[] = 'assign-users';
			remove_menu_page('publish-web');
			remove_menu_page('assign-users');
		}
	}
}

// redirects users for prevent acces to forbidden menu options
add_action( 'admin_init', 'wemcor_disallowed_admin_pages' );
function wemcor_disallowed_admin_pages() {
	if ( is_admin() ) {
		global $pagenow;
		$role = wemcor_get_role_user();

		$allowed = [
			'edit.php',
			'upload.php',
			'profile.php',
			'mis-sitios',
			'nuevo-sitio',
			'admin.php',
			'post-new.php',
			'edit-tags.php',
			'media-new.php',
			'customize.php',
			'post.php'
		];

		if( $role == 'student' ) {
			wemcor_redirect_is_not_allowed($pagenow, $allowed);
			//wemcor_redirect_is_not_owner($pagenow, $allowed);
		}

		if( $role == 'teacher' ) {
			//https://wp.test.digitaldemocratic.net/gsjzvn/wp-admin/edit.php?action=publishwebupdate
			$allowed[] = 'publish-web';
			$allowed[] = 'assign-users';
			$allowed[] = 'publishwebupdate';
			wemcor_redirect_is_not_allowed($pagenow, $allowed);
			//wemcor_redirect_is_not_owner($pagenow, $allowed);
		}

	}
}

function wemcor_get_role_user() {
	$get_users_obj = get_users(
		array(
		    'blog_id' => get_current_blog_id(),
		    'search'  => get_current_user_id()
		)
	);

	foreach($get_users_obj[0]->roles as $role) {
		return $role;
		break;
	}

}

function wemcor_redirect_is_not_allowed($pagenow, $allowed) {
	//wp_redirect( get_admin_url($id, 'post.php?post='.$post_id.'&action=edit') );
	if ( $pagenow == 'admin.php' && isset( $_GET['page'] ) && !in_array($_GET['page'], $allowed) ) {
		wp_redirect( admin_url('/admin.php?page=mis-sitios') );//https://wp.test.digitaldemocratic.net/wp-admin/admin.php?page=mis-sitios
		exit();
	}

	if ( !in_array($pagenow, $allowed) ) {
		wp_redirect( admin_url('/admin.php?page=mis-sitios') );//https://wp.test.digitaldemocratic.net/wp-admin/admin.php?page=mis-sitios
		exit();
	}

}

function wemcor_redirect_is_not_owner($pagenow, $allowed) {
	// get blog_name from url for determinate blog_id
	// wemcor_get_owner($blog_id);
	// if (!$owner && $pagenow != 'mis-sitios') {
	// 	wp_redirect( admin_url('/admin.php?page=mis-sitios') );//https://wp.test.digitaldemocratic.net/wp-admin/admin.php?page=mis-sitios
	// 	exit();
	// }
}


function wemcor_get_owner($blog_id) {
	//$blog_id = get_current_blog_id();
	$user_id = get_current_user_id();

	$owner = get_blog_option( $blog_id, 'owner_user' );
	if( $owner == $user_id ) return true;

	return false;
}

