<?php
echo '<style type="text/css">
		/*estilos admin menu*/
		#adminmenu, #adminmenu .wp-submenu, #adminmenuback, #adminmenuwrap {
			background-color: '. $bg_color .'!important;
		}
		#adminmenu li {
			border-bottom: 1px solid '. $secondary .';
			/*border-right: 1px solid #262626;*/
		}
		#adminmenu li.wp-menu-separator {
			padding-top: 6px;
			margin: 0;
			border-bottom: 0;
		}
		#adminmenu li.wp-menu-separator .separator, {
			border-bottom: 1px solid '. $secondary .';
		}
		/*#adminmenu li.wp-has-current-submenu + li.wp-menu-separator,
		#adminmenu li:hover + li.wp-menu-separator {
			background-color: '. $secondary .'!important;
		}*/
		#adminmenu li:hover div.wp-menu-image:before,
		#adminmenu li a:hover {
			color: '. $primary .';
		}
		#adminmenu li.wp-not-current-submenu:hover > a,
		#adminmenu li:hover > a {
			background-color: '. $secondary .'!important;
		}

		#adminmenu li.wp-has-current-submenu div.wp-menu-image:before {
			color: '. $primary .'!important;
		}

		#adminmenu a {
			font-size: 16px!important
		}
		#adminmenu a,
		#adminmenu div.wp-menu-image:before {
			color: #262626!important;
		}
		#adminmenu a:hover,
		#adminmenu a:hover div.wp-menu-image:before,
		#adminmenu li:hover  {
			color: '. $primary .'!important;
		}

		/*#adminmenu .wp-has-current-submenu  {
			border-right: 1px solid #262626;
		}*/
		#adminmenu .wp-has-current-submenu>a  {
			color: '. $primary .'!important;
			background-color: '. $secondary .'!important;
		}
		#adminmenu .wp-has-current-submenu .wp-submenu {
			padding: 0!important;
			background-color: '. $secondary .'!important;
		}
		#adminmenu .wp-has-current-submenu .wp-submenu li a {
			padding-top: 10px;
			padding-bottom: 10px;
			font-size: 14px !important;
		}
		ul#adminmenu a.wp-has-current-submenu:after,
		ul#adminmenu>li.current>a.current:after,
		ul#adminmenu a.wp-not-current-submenu:after {
			border-right-color: '. $primary .'!important;
			/*right: -1px;
			border-width: 9px;*/
		}
		/*ul#adminmenu a.wp-has-current-submenu:after,
		ul#adminmenu>li.current>a.current:after,
		ul#adminmenu a.wp-not-current-submenu:after,
		#adminmenu .wp-has-submenu:after {
			border-right-color: '. $primary .'!important;
			/*right: -1px;
			border-width: 9px;*/
		}*/
		#adminmenu li.wp-has-submenu.wp-not-current-submenu:hover:after {
			border-right-color: '. $primary .';
		}
		#adminmenu .wp-submenu li {
			border-bottom: 0;
		}
		body.wp-admin #adminmenu li.wp-has-submenu.wp-not-current-submenu,
		body.wp-admin #adminmenu li.wp-has-submenu.wp-not-current-submenu > a {
			background-color: '. $bg_color .'!important;
		}
		body.wp-admin #adminmenu li.wp-has-submenu.wp-not-current-submenu:hover {
			background-color: '. $secondary .'!important;
		}
		#adminmenu li.wp-has-submenu.wp-not-current-submenu .wp-submenu a {
			color: #262626!important;
		}
		#adminmenu li.wp-has-submenu.wp-not-current-submenu .wp-submenu li a:hover {
			color: '. $primary .'!important;
		}
		#collapse-button {
			font-size: 14px;
			margin-top: 10px;
		}
		#collapse-button:hover {
			color: '. $primary .';
			opacity: 0.7;
		}
		#adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head,
		#adminmenu .wp-not-current-submenu .wp-submenu .wp-submenu-head {
			color: #262626;
			/*font-size: 16px;
			font-weight: bold;*/
		}

		#adminmenu .wp-has-submenu.wp-not-current-submenu.menu-top .wp-menu-name {
			background-color: '. $bg_color .'!important;
		}
		#adminmenu .current.menu-top a,
		#adminmenu .current.menu-top a .wp-menu-image:before {
			color: '. $primary .'!important;
		}
		#adminmenu .wp-has-submenu.wp-not-current-submenu.menu-top .wp-menu-name:hover,
		#adminmenu .wp-not-current-submenu.menu-top:hover {
			background-color: '. $secondary .'!important;
		}
		#adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head, #adminmenu .wp-menu-arrow, #adminmenu .wp-menu-arrow div, #adminmenu li.current a.menu-top, #adminmenu li.wp-has-current-submenu a.wp-has-current-submenu, .folded #adminmenu li.current.menu-top, .folded #adminmenu li.wp-has-current-submenu {
			background-color: '. $secondary .';
		}

.page #main .entry-header {
display: none!important;
}
.page.singular .entry-title {
display: none;
}
.singular .entry-header {
border: 0;
padding: 0;
margin: 0;
}


		/*espacios*/
		html {
			padding-top: 32px;
		}
		html.wp-toolbar {
			padding-top: 66px;/*mismo espacio que altura logo*/
		}

		/*admin bar*/
		#wpadminbar {
			height: auto;
			background-color: '. $bg_color .';
			padding: 0 30px;
			border-bottom: 1px solid #000;
			margin-bottom: 20px;
			box-sizing: border-box;
		}

		/* wp-logo*/
		#wpadminbar #wp-admin-bar-wp-logo > .ab-item {
			width: 200px;
			height: 66px;
			display: flex;
			padding: 0;
		}
		#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
			content: none;
		}
		#wpadminbar>#wp-toolbar>#wp-admin-bar-root-default .ab-icon {
			background-image: url(' . $bg_logo . ') !important;
			background-position: center;
			background-repeat: no-repeat;
			background-size: contain;
			width: 100%;
			height: 100%;
			float: none;
			padding: 0;
		}
		#wpadminbar .ab-top-menu>li.hover>.ab-item,
		#wpadminbar.nojq .quicklinks .ab-top-menu>li>.ab-item:focus,
		#wpadminbar:not(.mobile) .ab-top-menu>li:hover>.ab-item,
		#wpadminbar:not(.mobile) .ab-top-menu>li>.ab-item:focus {
			background-color: '. $bg_color .'!important;
		}

		/*dropdown menu*/
		#wpadminbar .fas,
		#wpadminbar .far {
			font-family: "Font Awesome 5 Free"!important;
		}
		#wpadminbar .fa {
			font-family: "FontAwesome";
		}
		#wpadminbar .fab {
    		font-family: "Font Awesome 5 Brands"
		}
		#wpadminbar .ab-top-menu #wp-admin-bar-dropdown-menu.menupop .ab-sub-wrapper {
			min-width: 295px;
			max-height: 500px;
			overflow-y: scroll;
			border: 1px solid rgba(0,0,0,0.15);
			border-radius: 5px;
			box-shadow: rgba(0,0,0,0.2) 0 3px 8px;
			top: 60px!important;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu {
			display: flex;
			height: 66px;
			align-items: center;
			margin-right: 40px;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu > .ab-item {
			background-image: url(' . plugin_dir_url(__FILE__) . 'apps.svg);
			background-repeat: no-repeat;
			background-size: contain;
			width: 38px;
			height: 38px;
			padding: 0;
			cursor: pointer;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul {
			display: flex;
			flex-wrap: wrap;
			padding-top: 20px;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul#app-external {
			padding-top: 0;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li {
			text-align: center;
			width: 33%;
			padding: 0 20px 0;
			margin: 0 0 13px;
			box-sizing: border-box;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a {
			min-width: auto;
			padding: 0;
			height: 100%;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a .icon {
			width: 40px;
			margin: auto;
			background-color: ' . $primary .';
			border-radius: 4px;
			height: 40px;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a .icon i {
			font-size: 19px;
			line-height: 40px;
			color: white;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps hr {
			width: 310px;
			margin: 0 auto 20px;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a .text {
			font-size: 13px;
			font-weight: 300;
			line-height: 15px;
			margin-top: 5px;
		}
		#wpadminbar #wp-admin-bar-dropdown-menu #wp-admin-bar-menu-apps ul > li > a.ab-item {
			white-space: break-spaces;
		}

		/*notificaciones*/
		#wpadminbar #wp-admin-bar-notifications {
			display: flex;
			height: 66px;
			align-items: center;
			margin-right: 40px;
		}
		#wpadminbar #wp-admin-bar-notifications > .ab-item {
			background-image: url(' . plugin_dir_url(__FILE__) . 'bell.svg);
			background-repeat: no-repeat;
			background-size: contain;
			width: 32px;
			height: 32px;
			padding: 0;
			cursor: pointer;
		}
		/*counter*/
		#wpadminbar #wp-admin-bar-notifications .counter {
			position: absolute;
			top: 10px;
			right: -13px;
			background-color: red;
			width: 20px;
			height: 22px;
			border-radius: 6px;
			color: white;
			font-size: 10px;
			text-align: center;
			line-height: 22px;
		}

		/*mis sitios*/
		#wpadminbar #wp-admin-bar-my-sites,
		#wpadminbar #wp-admin-bar-mis-sitios {
			display: flex;
			height: 66px;
			align-items: center;
			margin-left: 30px;
		}
		#wpadminbar #wp-admin-bar-my-sites a,
		#wpadminbar #wp-admin-bar-my-sites a:before,
		#wpadminbar #wp-admin-bar-mis-sitios a,
		#wpadminbar #wp-admin-bar-mis-sitios a:before {
			color: #262626!important;
		}
		#wpadminbar #wp-admin-bar-my-sites .ab-sub-wrapper a:hover,
		#wpadminbar #wp-admin-bar-my-sites .ab-sub-wrapper a:hover:before,
		#wpadminbar #wp-admin-bar-mis-sitios .ab-sub-wrapper a:hover,
		#wpadminbar #wp-admin-bar-mis-sitios .ab-sub-wrapper a:hover:before {
			color: '. $primary .'!important;
			background-color: '. $secondary.';
		}
		#wpadminbar #wp-admin-bar-my-sites a.ab-item:before,
		#wpadminbar #wp-admin-bar-mis-sitios a.ab-item:before {
			top: 0;
			font-size: 32px;
			line-height: 66px;
			padding: 0;
		}
		#wpadminbar #wp-admin-bar-mis-sitios:before {
			/*content: url(' . plugin_dir_url(__FILE__) . 'layout.svg);*/
			content: "\f116";
			font-family: dashicons;
			font-size: 34px;
			color: #000;
			line-height: 1em;
		}
		/*#wpadminbar #wp-admin-bar-mis-sitios a.ab-item:before {
			line-height: 57px;
		}*/
		#wpadminbar #wp-admin-bar-my-sites a.ab-item,
		#wpadminbar #wp-admin-bar-mis-sitios a.ab-item {
			height: 66px;
			line-height: 66px;
			font-size: 16px;
		}
		#wpadminbar .menupop .menupop>.ab-item .wp-admin-bar-arrow:before {
			line-height: 50px;
			padding: 0;
			top: 0;
		}
		#wpadminbar .ab-sub-wrapper li.menupop {
			height: 50px;
			line-height: 50px;
			background-color: '. $bg_color .';
		}
		#wpadminbar .quicklinks .menupop ul.ab-sub-secondary,
		#wpadminbar .quicklinks .menupop ul.ab-sub-secondary .ab-submenu {
			background-color: '. $bg_color .'!important;
		}
		#wpadminbar .ab-sub-wrapper li.menupop a.ab-item {
			height: 50px!important;
			line-height: 50px!important;
		}
		#wpadminbar .ab-sub-wrapper li.menupop a.ab-item:hover {
			color: '. $primary .'!important;

		}
		#wpadminbar .ab-sub-wrapper .ab-submenu {
			padding: 0;
		}
		#wpadminbar .ab-top-menu .menupop .ab-sub-wrapper .ab-sub-wrapper {
			top: 30px!important;
		}
		#wpadminbar .quicklinks li .blavatar:before {
			color: #262626!important;
		}
		#wpadminbar .quicklinks li a:hover .blavatar:before {
			color: '. $primary .'!important;
			background-color: '. $secondary.';
		}

		/*usuario*/
		#wpadminbar #wp-admin-bar-my-account {
			display: flex;
			height: 66px;
			align-items: center;
			margin-right: 40px;
		}
		#wpadminbar #wp-admin-bar-my-account img {
			cursor: pointer;
		}
		#wpadminbar .ab-top-menu .menupop .ab-sub-wrapper,
		#wpadminbar .ab-top-secondary .menupop .ab-sub-wrapper {
			top: 66px;
			background: '. $bg_color. ';
		}

		#wpadminbar .ab-top-secondary .menupop .ab-sub-wrapper a {
			color: #262626!important;
		}
		#wpadminbar .ab-top-secondary .menupop .ab-sub-wrapper a:hover {
			color: '. $primary .'!important;
			background-color: '. $secondary.';
		}

		#wpadminbar .ab-top-secondary #wp-admin-bar-dropdown-menu .ab-sub-wrapper a:hover {
			background-color: transparent;
		}

		#wpadminbar #wp-admin-bar-my-account > .ab-item {
			padding: 0;
			height: auto;
		}

		#wpadminbar #wp-admin-bar-my-account.with-avatar > .ab-item img {
			border: 0;
			height: 42px;
			width: 42px;
			border-radius: 90px;
		}

		@media screen and (max-width: 782px) {
			html #wpadminbar {
				height: 70px;
				position: fixed;
			}
			.wp-responsive-open #wpadminbar #wp-admin-bar-menu-toggle .ab-item {
				background-color: '. $bg_color .';
			}
			.wp-responsive-open #wpadminbar #wp-admin-bar-menu-toggle .ab-icon:before {
				color: '. $primary .';
			}
			#wpadminbar #wp-admin-bar-menu-toggle .ab-icon:before {
				line-height: 66px;
				color: #000;
			}
			#wpadminbar #wp-admin-bar-menu-toggle a {
				height: 66px;
			}
			#wpadminbar .quicklinks li#wp-admin-bar-my-account.with-avatar>a img {
				height: 50px;
				width: 50px;
				top: 10px;
				right: 0;
			}
			#wpadminbar .quicklinks li#wp-admin-bar-my-account.with-avatar>a {
				height: 66px;
			}
			#wpadminbar #wp-admin-bar-my-account {
				margin-right: 20px;
			}
			#wpadminbar #wp-admin-bar-user-info .display-name {
				color: #000;
			}
			#wpadminbar .ab-top-menu>.menupop>.ab-sub-wrapper .ab-item {
				padding: 0 16px;
			}
			#wpadminbar .ab-top-menu .menupop .ab-sub-wrapper .ab-sub-wrapper {
				top: 50px!important;
			}
			#wpadminbar #wp-admin-bar-mis-sitios a.ab-item {
				text-indent: -9999px;
			}

		}


	</style>';