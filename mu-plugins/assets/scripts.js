(function($) {
    $(document).ready(function() {
      $('.item-image-wrap').click(function(event) {
        $('.item-image-wrap .tick').hide();
        $('.tick', this).show();

        //get id theme
        var id_theme = $('img', this).attr('id');
        $('#theme_site').val(id_theme);
        //console.log($('#theme_site').val());
      });
    });
})(jQuery);


