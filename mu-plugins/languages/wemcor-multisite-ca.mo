��    M      �      �      �     �     �     
       	        (     6     B     O     [     s     �  �   �           %  P  @     �     �     �     �     �     �          
               '  
   6     A     H     P     S     j  	   {     �     �     �      �     �     �     �     	  ?   	     S	     [	     g	     s	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	  	   
  d   
  "   q
     �
     �
     �
  
   �
     �
  ;   �
       	          	   !     +     9     @     Q     U     h  �  n     U     f     z     �     �     �     �     �     �     �       	     k   )     �  (   �  �  �  	   N     X  .  v  �  �  L  Z     �     �     �     �     �     �                
               8     E     L     T      p  +   �     �     �     �  &   �  @        _     h     v     �     �     �     �     �     �     �     �       	     	     
      ~   +     �     �     �     �     �     �  D        U  	   ]     g     �     �     �     �     �     �     �   << Back to page Add new site Add new website All All pages Apply changes Assign User Assign Users Assign site Assign users sucesfully Assign users to site Assigned Barra de administración de Wordpress personalizada para mostrar estilos diferentes y menús de navegación a medida. Compatible con multisite Blog Changes saved successfully Check to assign user to this website. Uncheck the checkbox for a user to stop belonging to a site. The user is not deleted, they just no longer belong to the website. You can filter by groups or roles to find users more easily. Use the search engine to find users who match your search with their first name, last name or email address. Choose Theme Default theme: Theme 1 Description Theme 1 Description Theme 2 Description Theme 3 Dismiss this notice Edit Email Filter Filter by group Filter by role First name Groups Headers Id Invalid email address. Invalid site ID. Last name Manager Missing email address. Missing name site. Missing or invalid site address. My websites Network administrator New page Not found users Only lowercase letters (a-z), numbers, and hyphens are allowed. Publish Publish Web Publish web Pàgina d'exemple Required fields are marked %s Role Search Search users for Site Address (URL) Site Language Site Title Site: Student Teacher Templates The following words are reserved for use by WordPress functions and cannot be used as blog names: %s The requested site does not exist. Theme 1 Theme 2 Theme 3 Unassigned Unpublish web Upps!!! Critical error. Contact with the site administrator Visit Visit Web Visit all pages Visit web Website Pages Wemcor Wemcor Admin Bar at  https://wemcor.com users Project-Id-Version: wemcor-multisite
POT-Creation-Date: 2021-06-15 11:48+0200
PO-Revision-Date: 2021-08-26 19:19+0000
Last-Translator: 
Language-Team: Català
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;esc_attr_e
X-Poedit-SearchPath-0: .
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.5.2; wp-5.7.2 << Tornar enrere Afegir nou lloc web Afegir nou lloc web Tots Totes les pàgines Desa canvis Assigneu usuari Assigneu usuaris Assigneu lloc web Usuari assignat correctament Assigneu usuaris al lloc web Assignats Barra d'administració de WordPress per mostrar estils i navegació personalitzada compatible amb Multisite Blog Els canvis s’han fet satisfactoriament Marca el checkbox de cada usuari per assignar aquest lloc web. Desmarca el checkbox perquè un usuari deixi de pertànyer a un lloc. L'usuari no s'esborra, tan sols deixa de pertànyer a la pàgina web. Pots filtrar per grups o per rols per trobar amb més facilitat als usuaris. Fes servir el cercador per trobar usuaris que coincideixi la recerca amb el seu nom, cognoms o correu electrònic. Tria tema Tema per defecte: Tema Bàsic Un tema elegant, personalitzable, simple i fàcil de llegir — feu-ho vostre amb un menú, una imatge de capçalera i un fons personalitzat. Suporta sis àrees per a ginys (dues en la barra lateral, quatre al peu de pàgina) i té una plantilla de pàgina a una columna que suprimeix la barra lateral. Un tema amb un disseny adaptatiu que es veu correctament en qualsevol dispositiu. Les funcionalitats inclouen una plantilla de pàgina inicial amb els seus ginys, un visualitzador de font opcional, estils per als formats d’entrades tant a la pàgina inicial com a les vistes individuals, i una plantilla de pàgina sense barra lateral. Podeu configurar-lo al vostre gust amb un menú, una imatge de capçalera i un fons personalitzat. Un tema lleuger de WordPress construït amb un enfocament en la velocitat i usabilitat. Aqest tema us dóna més control sobre la creació del vostre contingut. Inclou 9 àrees de ginys, 5 ubicacions de navegació, 5 dissenys de barra lateral, menús desplegables (clic o flotant) i ajustos personalitzats de colors de navegació. 
 Rebutgeu aquest avís Editeu Correu electrònic Filtreu Filtreu per grup Filtreu per rol Nom Grups Títols i text Id Correu electrònic invàlid Invàlid ID  Cognom Manager Falta el correu electrònic Has de posar un nom al lloc web  Falta l’adreça del lloc o no és vàlida Els meus llocs web Administrador de la xarxa Nova pàgina No hi ha usuaris amb els filtres usats Només es permeten lletres minúscules (a-z), números i guions. Publicar Publiqueu web Publiqueu web Página d'example %s Camps obligatoris Rol Cercar Usuaris cercats per Adreça del lloc (URL) Idioma del lloc web Nom del lloc web Lloc: Estudiant Professor Plantillas Les següents paraules estan reservades per a ús de les funcions de WordPress i no es poden utilitzar com a noms de blocs: %s El lloc requerit no existeix. Tema bàsic Tema intermig Tema avançat Sense assignar Despubliqueu web Upps!!! Error de sistema. Contacteu amb l'admininstrador del sistema Visiteu Veure web Visiteu totes les pàgines Visiteu web Páginas del lloc web Wemcor Wemcor Admin Bar a les  https://wemcor.com usuaris 