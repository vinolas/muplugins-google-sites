<?php
/*
Plugin Name: Wemcor Admin Bar
Plugin URI:
Description: Barra de administración de Wordpress personalizada para mostrar estilos diferentes y menús de navegación a medida. Compatible con multisite
Author: Wemcor
Author URI: https://wemcor.com
Version: 2.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//añadimos items de menú personalizados
add_action( 'admin_bar_menu', 'wemcor_add_new_items_admin_bar', 999 );
function wemcor_add_new_items_admin_bar($admin_bar) {
	//notificaciones
    $args = array(
        'parent' => 'top-secondary',
        'id'     => 'notifications',
        'title'  => false,
        'href'   => false,
        'meta'   => array(
        	'onclick' => 'open-notifications',
        	'class' => 'notifications',
        	'html' => '<span class="counter">0</span>'
        )
    );
    $admin_bar->add_node( $args );

    $html = html_menu_apps();

	//apps
	$args = array(
        'parent' => 'top-secondary',
        'id'     => 'dropdown-menu',
        'title'  => false,
        'href'   => false,
        'meta'   => array(
        	'onclick' => 'openDropDown',
        	'class' => 'dropdown-menu menupop',
        	'html' => $html
        )
    );
    $admin_bar->add_node( $args );

    //mis sitios
	$args = array(
        'parent' => '',
        'id'     => 'mis-sitios',
        'title'  => __('My websites', 'wemcor-multisite'),
        'href'   => admin_url() . 'admin.php?page=mis-sitios',
        'meta'   => array(
        	'class' => 'mis-sitios'
        )
    );
    $admin_bar->add_node( $args );

}

//borramos todos los items de admin bar excepto logo y usuario que lo modificaremos posteriormente. De esta forma prevenimos la carga de ítems extras colocados por otros plugins que s epuedan instalar en un futuro
add_action( 'wp_before_admin_bar_render', 'wemcor_remove_menu_admin_bar', PHP_INT_MAX );
function wemcor_remove_menu_admin_bar() {
    global $wp_admin_bar;

    //JSON
    $path_json = wemcor_get_url_json();
    $json = file_get_contents($path_json);
	$json_data = json_decode($json, true);

    $no_clear_menus = array(
    	'wp-logo',
    	'my-account',
    	'user-actions',
    	'user-info',
    	'edit-profile',
    	'logout',
    	'menu-toggle',
    	'notifications',
    	'dropdown-menu',
    	'mis-sitios',
    	'top-secondary'
	);

	$all_menus = $wp_admin_bar->get_nodes();
	remove_nodes($no_clear_menus, $all_menus, $wp_admin_bar);
//print_r($all_menus);
	// current user
	$user_id = get_current_user_id();
	$user_data = get_user_by( 'id', $user_id );
	$user_name = $user_data->display_name;

	// modificación de ítems existentes
	$my_account = $wp_admin_bar->get_node( 'my-account' );
	if($my_account) {
		/*stdClass Object(
		    [id] => my-account
		    [title] => Hola, gestor
		    [parent] => top-secondary
		    [href] => http://test.wemcor.es/wp-admin/network/profile.php
		    [group] =>
		    [meta] => Array(
		        [class] => with-avatar
		    )
		)*/

		if( isset($json_data['user']['avatar']) ) $user_avatar = '<img src="'.$json_data['user']['avatar'].'"/>';
		else $user_avatar = get_avatar($user_id);
		if( isset($json_data['user']['account']) ) $user_account = $json_data['user']['account'];
		else $user_account = $my_account->href;

		$wp_admin_bar->add_node(
			array(
	            'parent'    => $my_account->parent,
	            'id'        => $my_account->id,
	            'title'     => $user_avatar,
	            'href'      => '',
	            'group' 	=> $my_account->group,
	            'meta' 		=> $my_account->meta
    		)
		);

		$logout_node = $wp_admin_bar->get_node( 'logout' );
		$logout_wp_nonce = explode('&', $logout_node->href);
		$count_explode = count($logout_wp_nonce) - 1;
		$href_logout = 'https://sso.'.ltrim(DOMAIN_CURRENT_SITE, 'wp.').'/auth/realms/master/protocol/openid-connect/logout?redirect_uri=https://wp.'.ltrim(DOMAIN_CURRENT_SITE, 'wp.').'&'.$logout_wp_nonce[$count_explode];
		//https://sso.DOMAIN_CURRENT_SITE/auth/realms/master/protocol/openid-connect/logout?redirect_uri=https%3A%2F%2Fwp.DOMAIN_CURRENT_SITE
		//https://sso.test.digitaldemocratic.net/auth/realms/master/protocol/openid-connect/logout?redirect_uri=https://wp.test.digitaldemocratic.net&_wpnonce=5b2c887063
		//https://wp.test.digitaldemocratic.net/wp-login.php?action=logout&_wpnonce=5b2c887063

		$wp_admin_bar->add_node(
			array(
	            'parent'    => $logout_node->parent,
	            'id'        => $logout_node->id,
	            'title'     => $logout_node->title,
	            'href'      => $href_logout,
	            'group' 	=> $logout_node->group,
	            'meta' 		=> $logout_node->meta
    		)
		);


	}

	$my_avatar = $wp_admin_bar->get_node( 'user-info' );
	if($my_avatar) {
		/*stdClass Object(
		    [id] => user-info
		    [title] => '<img alt="" src="https://secure.gravatar.com/avatar/6ecd73b47d0114688bc7b9c3b35fe019?s=64&amp;d=mm&amp;r=g" srcset="https://secure.gravatar.com/avatar/6ecd73b47d0114688bc7b9c3b35fe019?s=128&amp;d=mm&amp;r=g 2x" class="avatar avatar-64 photo" height="64" width="64" loading="lazy"><span class="display-name">wp-manager</span>'
		    [parent] => user-actions
		    [href] => https://wp.montseny.digitaldemocratic.net/wp-admin/profile.php
		    [group] =>
		    [meta] => Array(
            	[tabindex] => -1
        	)
		)*/
		if( isset($json_data['user']['avatar']) ) $user_avatar = '<img alt="" src="'.$json_data['user']['avatar'].'" srcset="'.$json_data['user']['avatar'].' 2x" class="avatar avatar-64 photo" height="64" width="64" loading="lazy"><span class="display-name">'.$user_name.'</span>';
		else $user_avatar = '<img alt="" src="'.get_avatar($user_id).'" srcset="'.get_avatar($user_id).' 2x" class="avatar avatar-64 photo" height="64" width="64" loading="lazy"><span class="display-name">'.$user_name.'</span>';
		if( isset($json_data['user']['account']) ) $user_account = $json_data['user']['account'];
		else $user_account = $my_account->href;
		$wp_admin_bar->add_node(
			array(
	            'id'     => $my_avatar->id,
	            'title'  => $user_avatar,
	            'parent' => $my_avatar->parent,
	            'href'   => $user_account,
	            'group'  => $my_avatar->group,
	            'meta' 	=> $my_avatar->meta
    		)
		);
	}

	$my_profile = $wp_admin_bar->get_node( 'edit-profile' );
	if($my_profile) {
		/*stdClass Object(
		    [id] => edit-profile
		    [title] => Edit Profile
		    [parent] => user-actions
		    [href] => https://wp.montseny.digitaldemocratic.net/wp-admin/profile.php
		    [group] =>
		    [meta] => Array()
		)*/
		if( isset($json_data['user']['account']) ) $user_account = $json_data['user']['account'];
		else $user_account = $my_account->href;
		$meta = $my_profile->meta;
		$meta['target'] = '_blank';
		$wp_admin_bar->add_node(
			array(
	            'id'     => $my_profile->id,
	            'title'  => $my_profile->title,
	            'parent' => $my_profile->parent,
	            'href'   => $user_account,
	            'group'  => $my_profile->group,
	            'meta' 	=> $meta
    		)
		);
	}

}

function remove_nodes($no_clear_menus, $all_menus, $wp_admin_bar) {
	foreach( $all_menus as $menu ) {
    	if( ! in_array($menu->id, $no_clear_menus) ) $wp_admin_bar->remove_menu( $menu->id );
    }
}


//estilos personalizados admin bar
add_action( 'wp_before_admin_bar_render', 'wemcor_custom_styles_admin_bar', 999 );
function wemcor_custom_styles_admin_bar() {
	//JSON
    $path_json = wemcor_get_url_json();
    $json = file_get_contents($path_json);
	$json_data = json_decode($json, true);

	if( isset($json_data['logo']) ) $bg_logo = $json_data['logo'];
	else $bg_logo = WPMU_PLUGIN_URL . '/custom-logo-admin-bar.png';
	//$bg_logo = WPMU_PLUGIN_URL . '/custom-logo-admin-bar.png';

	if( isset($json_data['colours']['background']) ) $bg_color = $json_data['colours']['secondary'];
	else $bg_color = '#FFF';

	if( isset($json_data['colours']['primary']) ) $primary = $json_data['colours']['primary'];
	else $primary = '#262626';

	if( isset($json_data['colours']['secondary']) ) $secondary = $json_data['colours']['background'];
	else $secondary = '#f0f0f0';

	require_once dirname(__FILE__) . '/assets/admin-bar-style.php';

}

//encolamos javascript para funciones de menu toggle
add_action( 'admin_enqueue_scripts', 'wemcor_admin_enqueue_scripts' );
add_action( 'wp_enqueue_scripts', 'wemcor_admin_enqueue_scripts' );
function wemcor_admin_enqueue_scripts() {

	//FontAwesome 5
	//wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css', '', '5.8.1', 'all');

	//FontAwesome 4
	wp_enqueue_style('fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', '', '4.7.0', 'all');

}


//HTML del menú de apps
function html_menu_apps() {
	//JSON
    $path_json = wemcor_get_url_json();
    $json = file_get_contents($path_json);
    $json_data = json_decode($json, true);

	/*
	Array(
	    [background_login] => https://api.montseny.digitaldemocratic.net/custom/img/background.png
	    [colours] => Array(
	        [background] => #F0F0F0
	        [primary] => #92AE01
	        [secondary] => #FFFFFF)
	    [logo] => https://api.montseny.digitaldemocratic.net/custom/img/logo.png
	    [apps_external] => Array(
	        [0] => Array(
	            [href] => https://agora.xtec.cat/ceipmontseny-barcelona/
	            [icon] => fa fa-university
	            [name] => Escola Web
	            [shortname] => web
	        )
            [1] => Array(
                [href] => https://youtube.com/
                [icon] => fa fa-youtube-play
                [name] => Youtube
                [shortname] => youtube)
            [2] => Array(
                [href] => https://www.wordreference.com/
                [icon] => fa fa-book
                [name] => Diccionari
                [shortname] => diccionari)
            [3] => Array(
                [href] => http://meet.jit.si/
                [icon] => fa fa-video-camera
                [name] => Reunions Jitsi
                [shortname] => jitsi)
            [4] => Array(
                [href] => https://www.google.es/
                [icon] => fa fa-search
                [name] => Cercar
                [shortname] => search)
            [5] => Array(
                [href] => https://www.google.es/maps/preview
                [icon] => fa fa-map-marker
                [name] => Maps
                [shortname] => maps)
	    )
	    [apps_internal] => Array(
            [0] => Array(
                [href] => https://nextcloud.montseny.digitaldemocratic.net/
                [icon] => fa fa-cloud
                [name] => Núvol + crear arxius
                [shortname] => cloud)
            [1] => Array(
                [href] => https://nextcloud.montseny.digitaldemocratic.net/apps/mail/setup
                [icon] => fa fa-envelope-o
                [name] => Correu
                [shortname] => email)
            [2] => Array(
                [href] => https://pad.montseny.digitaldemocratic.net/
                [icon] => fa fa-file-text-o
                [name] => Pads
                [shortname] => pads)
            [3] => Array(
                [href] => https://nextcloud.montseny.digitaldemocratic.net/apps/forms
                [icon] => fa fa-check-square-o
                [name] => Formularis
                [shortname] => forms)
            [4] => Array(
                [href] => https://nextcloud.montseny.digitaldemocratic.net/apps/polls
                [icon] => fa fa-bar-chart
                [name] => Enquestes
                [shortname] => feedback)
            [5] => Array(
                [href] => https://nextcloud.montseny.digitaldemocratic.net/apps/spreed
                [icon] => fa fa-commenting-o
                [name] => Xat
                [shortname] => chat)
            [6] => Array(
                [href] => https://nextcloud.montseny.digitaldemocratic.net/apps/calendar
                [icon] => fa fa-calendar
                [name] => Calendari
                [shortname] => schedule)
            [7] => Array(
                [href] => https://wp.montseny.digitaldemocratic.net/wp-login.php?saml_sso
                [icon] => fa fa-rss
                [name] => Webs
                [shortname] => webs)
            [8] => Array(
                [href] => https://nextcloud.montseny.digitaldemocratic.net/apps/bbb
                [icon] => fa fa-video-camera
                [name] => Reunions BBB
                [shortname] => meets_bbb)
            [9] => Array(
                [href] => https://nextcloud.montseny.digitaldemocratic.net/apps/photos
                [icon] => fa fa-file-image-o
                [name] => Fotos
                [shortname] => photos)
		)
	    [user] => Array(
            [account] => https://sso.montseny.digitaldemocratic.net/auth/realms/master/account
            [avatar] => https://sso.montseny.digitaldemocratic.net/auth/realms/master/avatar-provider
            [password] => https://sso.montseny.digitaldemocratic.net/auth/realms/master/password
        )
	)
*/
	$html = '';
	if(isset($json_data['apps_external'])) $apps_external = $json_data['apps_external'];
	else $apps_external = false;

	if(isset($json_data['apps_internal'])) $apps_internal = $json_data['apps_internal'];
	else $apps_internal = false;

	if(isset($json_data['apps_courses'])) $apps_courses = $json['apps_my_courses'];
	else $apps_courses = false;

	if( $apps_external || $apps_internal || $apps_courses ) $html .= '<div id="wp-admin-bar-menu-apps" class="ab-sub-wrapper">';

	//internals
	if( $apps_internal ) {
		$html .= '<ul id="app-apps" class="ab-submenu">';
		foreach($apps_internal as $app) {
			/*
			[0] => Array(
		        [href] => https://agora.xtec.cat/ceipmontseny-barcelona/
		        [icon] => fa fa-university
		        [name] => Escola Web
		        [shortname] => web
		    )
			*/
			$html .= '<li class="'.$app['shortname'].'"><a href="'.$app['href'].'" class="ab-item"><div class="icon"><i class="'.$app['icon'].'"></i></div><div class="text">'.$app['name'].'</div></a></li>';
		}
		$html .= '</ul>';
	}

	//externals
	if( $apps_external ) {
		if( $apps_internal ) $html .= '<hr>';
		$html .= '<ul id="app-external" class="ab-submenu">';
		foreach($apps_external as $app) {
			/*
			[0] => Array(
		        [href] => https://agora.xtec.cat/ceipmontseny-barcelona/
		        [icon] => fa fa-university
		        [name] => Escola Web
		        [shortname] => web
		    )
			*/
			$html .= '<li class="'.$app['shortname'].'"><a href="'.$app['href'].'" class="ab-item"><div class="icon"><i class="'.$app['icon'].'"></i></div><div class="text">'.$app['name'].'</div></a></li>';
		}
		$html .= '</ul>';
	}

	//courses
	if( $apps_courses ) {
		if( $apps_internal  || $apps_external) $html .= '<hr>';
		$html .= '<ul id="app-external" class="ab-submenu">';
		foreach($apps_courses as $app) {
			/*
			[0] => Array(
		        [href] => https://agora.xtec.cat/ceipmontseny-barcelona/
		        [icon] => fa fa-university
		        [name] => Escola Web
		        [shortname] => web
		    )
			*/
			$html .= '<li class="'.$app['shortname'].'"><a href="'.$app['href'].'" class="ab-item"><div class="icon"><i class="'.$app['icon'].'"></i></div><div class="text">'.$app['name'].'</div></a></li>';
		}
		$html .= '</ul>';
	}

	if( $apps_external || $apps_internal || $apps_courses ) $html .= '</div>';


	return $html;
}

function wemcor_get_url_json() {
	$path = str_replace('wp.', 'api.', DOMAIN_CURRENT_SITE);
    return 'https://'.$path.'/json';
}

