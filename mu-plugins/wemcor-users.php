<?php
/*
Plugin Name: Wemcor Users
Plugin URI:
Description: Sistema de configuración para asignar sites a usuarios y grupos de usuarios por site
Author: Wemcor
Author URI: https://wemcor.com
Version: 2.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


add_action( 'plugins_loaded', function () {
	if ( ! class_exists( 'WP_List_Table' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
		require_once plugin_dir_path(__FILE__) . 'includes/table-users.php';
		Wemcor_Users::get_instance();
	}
} );

//add_action( 'admin_menu', 'wemcor_add_menu_assign_users', 10 );
// function wemcor_add_menu_assign_users() {
// 	//$role = wemcor_get_role_user();
// 	//if( $role == 'student' ) return;

// 	$hook = add_menu_page(
// 		__('Assign Users', 'wemcor-multisite'),
// 		__('Assign Users', 'wemcor-multisite'),
// 		'read',
// 		'assign-users',
// 		'wemcor_assign_users_callback',
// 		'dashicons-superhero-alt',
// 		4
// 	);

// 	//add_action( "load-$hook", "ata_rem_screen_options" );
// 	$users_obj = new Users_List();
// }

// function ata_rem_screen_options() {
// 	$option = 'per_page';
// 	$args   = [
// 		'label'   => __('Assign Users', 'wemcor-multisite'),
// 		'default' => 20,
// 		'option'  => 'users_per_page'
// 	];

// 	add_screen_option( $option, $args );

// }


// function wemcor_assign_users_callback() {
// 	echo '<h1>Assign Users</h1>';

// 	//temporal  (pruebas)
// 	$_POST['s'] = 'tresipunt.com';

// 	wemcor_get_users();
// 	wemcor_get_users($_POST['s']);
// 	wemcor_get_groups();
// 	wemcor_get_groups_users();

// }

// function wemcor_get_users($search = false) {
// 	echo ($search) ? '<h2>Get Users for '.$search.'</h2>' : '<h2>Get All users</h2>';
// 	//el buscador busca en id, en email, en el first, en el last, en todos lados
// 	if(!$search) {
// 		$url = 'http://isard-sso-admin:9000/api/internal/users';
// 		$users = json_decode(file_get_contents($url), true);
// 	} else {
// 		$url = 'http://isard-sso-admin:9000/api/internal/users/filter';
// 		$data = [
//     		'text' => $search,
// 		];
// 		$users_txt = CallAPI("POST", $url, $data);
// 		$users = json_decode($users_txt, true);
// 	}
// 	wemcor_show_users($users);
// }

// function wemcor_get_groups() {
// 	echo '<h2>Get Groups</h2>';
// 	$url = 'http://isard-sso-admin:9000/api/internal/groups';
// 	$groups = json_decode(file_get_contents($url), true);
// 	wemcor_show_users($groups);
// }

// function wemcor_get_groups_users() {
// 	// Method: POST, PUT, GET etc
// 	// Data: array("param" => "value") ==> index.php?param=value
// 	echo '<h2>Get Users from group</h2>';
// 	$url = 'http://isard-sso-admin:9000/api/internal/group/users';
// 	$data = [
//     	"path" => "/teacher",
// 	];
// 	$users_txt = CallAPI("POST", $url, $data);
// 	$users = json_decode($users_txt, true);
// 	wemcor_show_users($users);
// }

// function CallAPI( $method, $url, $data = false ) {
// 	$curl = curl_init();

// 	switch ($method) {
// 	    case "POST":
// 	        curl_setopt($curl, CURLOPT_POST, 1);
// 	        if ($data) {
// 	            $payload = json_encode( $data );
// 	            curl_setopt($culr, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
// 	            curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
// 	        }
// 	        break;
// 	    //case "PUT":
// 	        //curl_setopt($curl, CURLOPT_PUT, 1);
// 	        //break;
// 	    //default:
// 	        //if ($data) $url = sprintf( "%s?%s", $url, http_build_query($data) );
// 	}

// 	// Optional Authentication:
// 	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
// 	curl_setopt($curl, CURLOPT_USERPWD, "username:password");

// 	curl_setopt($curl, CURLOPT_URL, $url);
// 	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// 	$result = curl_exec($curl);

// 	curl_close($curl);

// 	return $result;
// }

// function wemcor_show_users( $users ) {
// 	foreach ($users as $user ) {
// 		foreach( $user as $key => $value ) {
// 			if( ! is_array($user[$key]) ) {
// 				echo $key.': ' . $value . '<br>';
// 			} else {
// 				echo $key .': <ul>';
// 				foreach($user[$key] as $group) {
// 					echo '<li>'. $group .'</li>';
// 				}
// 				echo '</ul>';
// 			}
// 		}
// 		echo '<hr>';
// 	}
// }

add_action( 'admin_head', function () { ?>
	<style>
	#wemcor-wrap.wemcor-table-users table #assign,
	#wemcor-wrap.wemcor-table-users table .column-assign {
		text-align: center;
	}
	</style>
<?php } );
