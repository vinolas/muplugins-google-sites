<?php
/*
Plugin Name: Wemcor Sites
Plugin URI:
Description: Muestra un dashboard al estilo Google sites con un listado de los sites disponibles para cada usuario y las páginas creadas
Author: Wemcor
Author URI: https://wemcor.com
Version: 2.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// añadir menus
add_action( 'admin_menu', 'wemcor_add_menu_mis_sitios', 10 );
function wemcor_add_menu_mis_sitios() {
	//mis sitios web
	add_menu_page(
		__('My websites', 'wemcor-multisite'),
		__('My websites', 'wemcor-multisite'),
		'read',
		'mis-sitios',
		'wemcor_mis_sitios_callback',
		'dashicons-welcome-widgets-menus',
		2
	);

	//nuevo sitio web
	add_menu_page(
		__('Add new website', 'wemcor-multisite'),
		__('Add new website', 'wemcor-multisite'),
		'read',
		'nuevo-sitio',
		'wemcor_nuevo_sitio_callback',
		//'dashicons-index-card',
		'dashicons-table-col-before',
		3
	);

	//administrador red (admin network). Lo sacamos de admin bar y lo ponemos en menu lateral
	add_menu_page(
		__('Network administrator', 'wemcor-multisite'),
		__('Network administrator', 'wemcor-multisite'),
		'manage_sites',
		'network',
		'',
		'dashicons-wordpress',
		1
	);

}

function wemcor_mis_sitios_callback() {
	require_once plugin_dir_path(__FILE__) . 'includes/mis-sitios.php';
}

function wemcor_nuevo_sitio_callback() {
	require_once plugin_dir_path(__FILE__) . 'includes/nuevo-sitio.php';
}

// Borrar menú escritorio para Teachers y Students
add_action( 'admin_init', 'remove_dashboard_menu_according_role' );
function remove_dashboard_menu_according_role() {
	$user = wp_get_current_user();
	if ( in_array( 'teacher', $user->roles ) || in_array( 'student', $user->roles ) ) remove_menu_page('index.php');

	//remove_menu_page('index.php');
}

//redirigir al iniciar sesión a cada usuario a esta página
add_filter( 'login_redirect', 'wemcor_redirect_mis_sitios_web' );
function wemcor_redirect_mis_sitios_web() {
	return admin_url() .'admin.php?page=mis-sitios';//http://test.wemcor.es/wp-admin/?page=mis-sitios
}

//add_action( 'admin_head', 'wemcor_change_icon_sitios_web' );
function wemcor_change_icon_sitios_web() {

	echo '<style>
	.toplevel_page_mis-sitios .wp-menu-image > img {
		width: 16px;
		opacity: 1;
	}
	</style>';
}

add_action( 'admin_head', 'wemcor_styles_mis_sitios' );
function wemcor_styles_mis_sitios() {

	echo '<style>
	/*.toplevel_page_mis-sitios .mi-sitio {
		margin: 15px 10px;
		width: calc(100% - 20px);
	}
	@media only screen and (min-width: 600px) {
		.toplevel_page_mis-sitios .mi-sitio {
			width: calc(44% - 20px);
		}
	}
	@media only screen and (min-width: 700px) {
		.toplevel_page_mis-sitios .mi-sitio {
			width: calc(27.33% - 20px);
		}
	}
	@media only screen and (min-width: 1024px) {
		.toplevel_page_mis-sitios .mi-sitio {
			width: calc(21% - 20px);
		}
	}*/
	/*.toplevel_page_mis-sitios .mi-sitio {
		border: 1px solid rgba(0,0,0,0.5);
		box-sizing: border-box;
	}*/
	.my-sites.striped li:after {
		background: #000;
	}

	</style>';
}



