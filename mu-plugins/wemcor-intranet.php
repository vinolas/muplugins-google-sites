<?php
/*
Plugin Name: Wemcor Intranet
Plugin URI:
Description: Sistema de configuración para publicar sites o mantenerlos en modo intranet (sólo usuarios con sesión iniciada)
Author: Wemcor
Author URI: https://wemcor.com
Version: 2.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*
 * Blog Privado por defecto
 *
 * Cuando se crea un sitio debemos crearlo como privado (intranet). Al usuario que intente ir a la home se le redirigirá a wp-login
 *
 */
//add_action( 'wp_insert_site', 'wemcor_change_default_attribute_new_site' );
function wemcor_change_default_attribute_new_site( $new_site ) {
	$blog_id = $new_site->blog_id;// Object WP_Site
	add_blog_option( $blog_id, 'wemcor-publishweb', false );
}

/*
 * Pestaña adicional en Site info
 *
 * Añadimos una pestaña de opciones en Site info (editar site) para disponer de la opción de publicar / despublicar site
 *
 */
add_filter( 'network_edit_site_nav_links', 'wemcor_custom_siteinfo_tabs' );
function wemcor_custom_siteinfo_tabs( $tabs ){

	$tabs['site-wemcor'] = array(
		'label' => __('Publish web', 'wemcor-multisite'),
		'url' => 'sites.php?page=publishweb',
		'cap' => 'manage_sites'
	);
	return $tabs;

}

// Creamos menu en barra lateral
add_action( 'network_admin_menu', 'wemcor_page_publicar_web' );
function wemcor_page_publicar_web() {

	add_submenu_page(
		'sites.php',
		__('Publish Web', 'wemcor-multisite'),
		__('Publish Web', 'wemcor-multisite'),
		'manage_sites',
		'publishweb',
		'callback_publicar_web'
	);

}

add_action( 'admin_menu', 'wemcor_add_menu_publish_web', 10 );
function wemcor_add_menu_publish_web() {
	add_menu_page(
		__('Publish web', 'wemcor-multisite'),
		__('Publish web', 'wemcor-multisite'),
		'read',
		'publish-web',
		'wemcor_publish_web_callback',
		'dashicons-welcome-view-site',
		4
	);
}

// Mostrar página (callback function)
function callback_publicar_web() {
	//var_dump($_REQUEST);

	$id = $_REQUEST['id'];

	//$title = 'Publicar Web';
	$current_blog_details = get_blog_details( array( 'blog_id' => $id ) );
	$site_name = $current_blog_details->blogname;

	echo '<div class="wrap"><h1 id="edit-site">' . __('Publish web', 'wemcor-multisite') . '</h1>
	<p class="edit-site-actions"><a href="' . esc_url( get_home_url( $id, '/' ) ) . '">'. __('Visit Web', 'wemcor-multisite') .'</a> | <a href="' . esc_url( admin_url() ) . 'edit.php?post_type=page">'. __('Visit all pages', 'wemcor-multisite') .'</a></p>';

		// navigation tabs
		network_edit_site_nav( array(
			'blog_id'  => $id,
			'selected' => 'site-wemcor'
		) );

		//output HTML
		echo '
		<style>
		#menu-site .wp-submenu li.wp-first-item{
			font-weight:600;
		}
		#menu-site .wp-submenu li.wp-first-item a{
			color:#fff;
		}
		/*#wemcor-publishweb{
			width: 1.25rem;
			height: 1.25rem;
		}
		#wemcor-publishweb:checked:before {
			/*content: "";*/
			background-color: #135e96;
			border-color: #135e96;
			width: 1.25rem;
			height: 1.25rem;
			border-radius: 4px;
			margin: -0.75px 0 0 -1px;
		}*/
	}
		</style>
		<form method="post" action="edit.php?action=publishwebupdate">';
			wp_nonce_field( 'wemcor-check' . $id, 'nonce' );

			echo '<input type="hidden" name="id" value="' . $id . '" />
			<table class="form-table">
				<tr>
					<th scope="row"><label for="wemcor-publishweb">'. __('Publish', 'wemcor-multisite') .' '. $site_name.'</label></th>
					<td><input name="wemcor-publishweb" class="regular-text" type="checkbox" id="wemcor-publishweb" value="1" ' . checked( get_blog_option( $id, 'wemcor-publishweb'), 1, false) . '/>
				</tr>
			</table>';

			submit_button();
		echo '</form></div>';

}

function wemcor_publish_web_callback() {
	//array(5) { ["nonce"]=> string(10) "f101813fd7" ["_wp_http_referer"]=> string(36) "/wp-admin/admin.php?page=publish-web" ["id"]=> string(1) "1" ["wemcor-publishweb"]=> string(1) "1" ["submit"]=> string(15) "Desa els canvis" }
	if( isset($_POST['submit']) && isset($_POST['id']) ) {
		wp_verify_nonce( $_POST['nonce'], 'wemcor-check' . $_POST['id']);

		if( isset($_POST['id']) && wp_verify_nonce( $_POST['nonce'], 'wemcor-check' . $_POST['id']) ) {
			if( isset($_POST['wemcor-publishweb'] ) ) $value = $_POST['wemcor-publishweb'];
			else $value = 0;
			$blog_id = $_POST['id'];
			update_blog_option( $blog_id, 'wemcor-publishweb', $value );
		}
	}


	$id = get_current_blog_id();

	//$title = 'Publicar Web';
	$current_blog_details = get_blog_details( array( 'blog_id' => $id ) );
	$site_name = $current_blog_details->blogname;

	echo '<div class="wrap"><h1 id="edit-site">' . __('Publish web', 'wemcor-multisite') . '</h1>
	<p class="edit-site-actions"><a href="' . esc_url( get_home_url( $id, '/' ) ) . '">'. __('Visit Web', 'wemcor-multisite') .'</a> | <a href="' . esc_url( admin_url() ) . 'edit.php?post_type=page">'. __('Visit all pages', 'wemcor-multisite') .'</a></p>';

	//output HTML
	echo '
		<style>
			#menu-site .wp-submenu li.wp-first-item{
				font-weight:600;
			}
			#menu-site .wp-submenu li.wp-first-item a{
				color:#fff;
			}
			/*#wemcor-publishweb{
				width: 1.25rem;
				height: 1.25rem;
			}
			#wemcor-publishweb:checked:before {
				/*content: "";*/
				background-color: #135e96;
				border-color: #135e96;
				width: 1.25rem;
				height: 1.25rem;
				border-radius: 4px;
				margin: -0.75px 0 0 -1px;
			}*/
		}
	</style>
	<form method="post">';
		wp_nonce_field( 'wemcor-check' . $id, 'nonce' );

		echo '<input type="hidden" name="id" value="' . $id . '" />
		<table class="form-table">
			<tr>
				<th scope="row"><label for="wemcor-publishweb">'. __('Publish', 'wemcor-multisite') .' '. $site_name.'</label></th>
				<td><input name="wemcor-publishweb" class="regular-text" type="checkbox" id="wemcor-publishweb" value="1" ' . checked( get_blog_option( $id, 'wemcor-publishweb'), 1, false) . '/>
			</tr>
		</table>';

		submit_button();

	echo '</form></div>';

}

// Guardar opción en base de datos
add_action( 'network_admin_edit_publishwebupdate',  'wemcor_save_publicar_web' );
function wemcor_save_publicar_web() {
	$blog_id = $_POST['id'];
	wp_verify_nonce( $_POST['nonce'], 'wemcor-check' . $blog_id);
	//check_admin_referer('wemcor-check'.$blog_id);
	update_blog_option( $blog_id, 'wemcor-publishweb', $_POST['wemcor-publishweb'] );

	wp_redirect( add_query_arg( array(
		'page' => 'publishweb',
		'id' => $blog_id,
		'updated' => 'true'), network_admin_url('sites.php')
	));//redirect to /wp-admin/sites.php?page=publishweb&blog_id=ID&updated=true

	exit;
}

// Notices
add_action( 'network_admin_notices', 'wemcor_notice' );
function wemcor_notice() {
	if( isset( $_GET['updated'] ) && isset( $_GET['page'] ) && $_GET['page'] == 'publishweb' ) {
		echo '<div id="message" class="updated notice is-dismissible">
			<p>'. __('Changes saved successfully', 'wemcor-multisite') .'</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">'. __('Dismiss this notice', 'wemcor-multisite').'</span></button>
		</div>';
	}
}

// Validaciones
add_action( 'current_screen', 'wemcor_redirects' );
function wemcor_redirects(){
	$screen = get_current_screen();
	if( $screen->id !== 'sites_page_publishweb-network' ) {
		return;
	}

	$id = isset( $_REQUEST['id'] ) ? intval( $_REQUEST['id'] ) : 0;

	if ( ! $id ) {
		wp_die( __('Invalid site ID.', 'wemcor-multisite') );
	}

	$details = get_site( $id );
	if ( ! $details ) {
		wp_die( __( 'The requested site does not exist.', 'wemcor-multisite' ) );
	}

	// if ( ! can_edit_network( $details->site_id ) ) {
	// 	wp_die( __( 'Sorry, you are not allowed to access this page.', 'wemcor-multisite' ), 403 );
	// }
}

// Esconder menu en barra lateral (ya se muestra en tabs de site info)
add_action( 'admin_head', 'wemcor_hide_publicar_web' );
function wemcor_hide_publicar_web() {

	echo '<style>
	#menu-site .wp-submenu li:last-child{
		display:none;
	}
	</style>';
}

//leemos opción guardada en la base de datos. Si es false se hará visible el site. Si es true se redirigirá a wp-login
add_action( 'template_redirect', 'wemcor_make_private_blog' );
function wemcor_make_private_blog() {
	$id_blog = get_current_blog_id();
	$private_blog = get_blog_option($id_blog, 'wemcor-publishweb', 0);

	if( !is_user_logged_in() && !$private_blog ) auth_redirect();
	// if( !is_user_logged_in() && !$private_blog ) {
	// 	wp_redirect('https://sso.montseny.digitaldemocratic.net/auth/realms/master/protocol/saml?SAMLRequest=hVLLbtswELz7KwLeJYqKrcKE7cCJ%2BzDg2kbs9tBLwFBrmwBf5VJ18%2FelpOZVICnBy87uDGcHnKAw2vN5E0%2F2Fn42gHFwkc5voy3yrjklTbDcCVTIrTCAPEq%2Bm39d8TIvuA8uOuk0%2BYf2PksgQojK2Z62XEzJZv1xtfm8XN8JEHBZjev7ejQsq0NVVKyQxWgITNSsLD5U4yEbyvG4p36HgElnSpIsGfRqiA0sLUZhY8KLkmXFKCuLPWO8TPfyR09dpGWVFbGjn2L0yClFdLlxNiLYh7xWRxWFrsE4GdKgzC1EKlJYNIDQBqkRGCHQxxRou3qvvv0LXStbK3t8P4%2F7fgj5l%2F1%2Bm203u30vMn%2FM6cZZbAyEHYRfSsK329Wz5bP%2Fn%2BOzz7Q7Kpv7k79qLd4JiWTWPTFpa95lFmapn7X1hL5En%2Bc8Xyfvy8XWaSUfOrw9n1wwIr69IstZh6g6O3SjvLHoQaqDgpo8ycy1dueblGyEKYmhAXJBZ4Pey%2Bs%2FOvsD&RelayState=%2Fwp-login.php&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=rDyFPOMZI04Xx8gBKUZJnm9E5f7ZK1OxXHaITky30js8CvuLZKN05hnmM9s5nROGqX7Hri4MDssiU47%2BF%2FOZHWWL6kngMaQ%2BNZNb9FWsA33r1HpyDUZYZYeGz0rT8aTgv%2BTmexAmtGXa3K3cO7lDDFSkyruFuMdlm2z9bidiKxGJT7BNeEA5D7W%2FXNHvMls0%2Faeq%2BgYIq0HZjlZPpuWcNNL5rGGpQYbU%2F5FYmZ8yy30Aohr2nUkbuif%2BHkyGup1jEBGjktHZamHveydXN18FULc9CsILt0N09tmvFMPa31oBz1%2Fcf0O%2BORJasSygE%2BYwJaBPFF6Pz9btfj6XNFA8Ng%3D%3D');
	// 	exit;
	// }
}
