<?php
/*
Plugin Name: Wemcor Patterns
Plugin URI:
Description: Carga patterns por defecto para Gutenberg
Author: Wemcor
Author URI: https://wemcor.com
Version: 2.0
Text Domain: wemcor-multisite
Domain Path:  /languages
*/

/*
 * Instrucciones nombramiento de ficheros
 *
 * Guardar los templates en formato .json con formato minusculas y palabras separadas con guión medio
 * El sistema se encarga de leer dicho directorio para mostrar todos los patterns. Si se desea dar de alta un nuevo pattern tan solo hay que guardar el .json en el directorio especificado
 * El sistema se encarga de hacer un rename para mostrar nombre del template
 * Se tienen que guardar en formato .json dentro de la carpeta mu-plugins/templates
 *
 */

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Registro categoria de patrones
add_action( 'init', 'wemcor_register_block_pattern_category' );
function wemcor_register_block_pattern_category () {
	if ( ! class_exists( 'WP_Block_Patterns_Registry') ) {
		return;
	}

	register_block_pattern_category (
		'wem-templates-headers',
		array(
			'label' => __('Headers', 'wemcor-multisite')
		)
	);

	register_block_pattern_category (
		'wem-templates-templates',
		array(
			'label' => __('Templates', 'wemcor-multisite')
		)
	);
}

// Registro de patrones
add_action( 'init', 'wemcor_register_block_patterns' );
function wemcor_register_block_patterns() {
	if ( ! class_exists( 'WP_Block_Patterns_Registry' ) ) {
		return;
	}

	//leemos directorio de templates para cargar todos los patterns
	require_once ABSPATH . 'wp-admin/includes/file.php';

	// $files = list_files( dirname(__FILE__) . '/templates/', 1);
	// foreach($files as $file) {
	// 	// /home/customer/www/test.wemcor.es/public_html/wp-content/mu-plugins/templates/template-fashion.json
	// 	$name = explode("/", $file);
	// 	wemcor_register_pattern( end($name) );
	// }

	$folders = list_files( dirname(__FILE__) . '/templates/', 1);
	foreach($folders as $folder) {
		// /var/www/html/wp-content/mu-plugins/templates/templates/
		$dirname = explode("/", $folder);
		array_pop($dirname);
		$files = list_files( $folder, 1);
		foreach( $files as $file ) {
			$name = explode("/", $file);
			wemcor_register_pattern( end($name), end($dirname) );
		}

	}

}

function wemcor_register_pattern( $file, $register_pattern ) {
	$url = dirname(__FILE__) . '/templates/' . $register_pattern . '/' . $file;
	$json = file_get_contents($url);
	$json_data = json_decode($json, true);
	$json_data['content'] = str_replace('%%DOMAIN%%', 'https://'.$_ENV["WORDPRESS_DOMAIN_CURRENT_SITE"].'/wp-content/mu-plugins/images', $json_data['content']);

	//rename
	$name = str_replace('-', ' ', $file);
	$name = str_replace('.json', '', $name);

	register_block_pattern(
		'wem-gutenberg-blocks-patterns/'.$file,
		array(
			'title'    => ucfirst($name),
			'content'  => $json_data['content'],
			'categories' => [ 'wem-templates-'.$register_pattern ],
		)
	);
}


